from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

db = SQLAlchemy()
ma = Marshmallow()


class recipe(db.Model):
    __tablename__ = "recipe"
    recipeid = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    totaltime = db.Column(db.Integer)
    description = db.Column(db.String)
    recipeyield = db.Column(db.Integer)
    imageurl = db.Column(db.String)
    author = db.Column(db.String)
    authorabout = db.Column(db.String)
    authorurl = db.Column(db.String)
    vegan = db.Column(db.Boolean)
    keto = db.Column(db.Boolean)
    sustainable = db.Column(db.Boolean)

    def __repr__(self):
        return "<Recipe %r>" % self.recipeid


class recipe_schema(ma.ModelSchema):
    class Meta:
        model = recipe


class recipeCategory(db.Model):
    __tablename__ = "recipecategory"
    recipeid = db.Column(db.Integer)
    category = db.Column(db.String)
    __table_args__ = (db.PrimaryKeyConstraint(recipeid, category),)

    def __repr__(self):
        return "<Recipe Category %r>" % self.recipeid


class recipeCategory_schema(ma.ModelSchema):
    class Meta:
        model = recipeCategory


class recipeStep(db.Model):
    __tablename__ = "recipestep"
    recipeid = db.Column(db.Integer, db.ForeignKey("recipe.recipeid"), nullable=False)
    step = db.Column(db.Integer)
    description = db.Column(db.String)
    __table_args__ = (db.PrimaryKeyConstraint(recipeid, step, description),)

    def __repr__(self):
        return "<Recipe Step %r>" % self.recipeid


class recipeStep_schema(ma.ModelSchema):
    class Meta:
        model = recipeStep


class recipeIngredient(db.Model):
    __tablename__ = "recipeingredient"
    recipeid = db.Column(db.Integer, db.ForeignKey("recipe.recipeid"), nullable=False)
    foodid = db.Column(db.Integer)
    amount = db.Column(db.Float)
    unit = db.Column(db.String)
    __table_args__ = (db.PrimaryKeyConstraint(recipeid, foodid, amount, unit),)

    def __repr__(self):
        return "<Recipe Ingredient %r>" % self.recipeid


class recipeIngredient_schema(ma.ModelSchema):
    class Meta:
        model = recipeIngredient


class food(db.Model):
    __tablename__ = "food"
    foodid = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    amount = db.Column(db.Float)
    unit = db.Column(db.String)


class food_schema(ma.ModelSchema):
    class Meta:
        model = food


class food_nutrition(db.Model):
    __tablename__ = "food_nutrition"
    foodid = db.Column(db.Integer, db.ForeignKey("food.foodid"))
    name = db.Column(db.String)
    amount = db.Column(db.Float)
    unit = db.Column(db.String)
    __table_args__ = (db.PrimaryKeyConstraint(foodid, name, amount, unit),)


class food_nutrition_schema(ma.ModelSchema):
    class Meta:
        model = food_nutrition


class locations(db.Model):
    __tablename__ = "locations"
    locationid = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    address = db.Column(db.String)
    description = db.Column(db.String)
    image = db.Column(db.String)
    number = db.Column(db.String)
    rating = db.Column(db.Integer)
    price = db.Column(db.Integer)
    storetype = db.Column(db.String)


class location_schema(ma.ModelSchema):
    class Meta:
        model = locations


class location_items(db.Model):
    __tablename__ = "location_items"
    locationid = db.Column(db.Integer)
    foodid = db.Column(db.Integer)
    __table_args__ = (db.PrimaryKeyConstraint(locationid, foodid),)


class location_items_schema(ma.ModelSchema):
    class Meta:
        model = location_items


class location_recipes(db.Model):
    __tablename__ = "location_recipes"
    locationid = db.Column(db.Integer)
    recipeid = db.Column(db.Integer)
    __table_args__ = (db.PrimaryKeyConstraint(locationid, recipeid),)


class location_recipes_schema(ma.ModelSchema):
    class Meta:
        model = location_recipes


"""
views
"""


class food_with_macros(db.Model):
    __tablename__ = "food_with_macros"
    foodid = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    amount = db.Column(db.Float)
    unit = db.Column(db.String)
    calorie = db.Column(db.Float)
    protein = db.Column(db.Float)
    fat = db.Column(db.Float)
    carb = db.Column(db.Float)


class food_with_macros_schema(ma.ModelSchema):
    class Meta:
        model = food_with_macros


class recipe_with_categories(db.Model):
    __tablename__ = "recipe_with_categories"
    recipeid = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    totaltime = db.Column(db.Integer)
    description = db.Column(db.String)
    recipeyield = db.Column(db.Integer)
    imageurl = db.Column(db.String)
    author = db.Column(db.String)
    authorabout = db.Column(db.String)
    authorurl = db.Column(db.String)
    vegan = db.Column(db.Boolean)
    keto = db.Column(db.Boolean)
    sustainable = db.Column(db.Boolean)
    categories = db.Column(db.Text)

    def __repr__(self):
        return "<Recipe %r>" % self.recipeid


class recipe_with_categories_schema(ma.ModelSchema):
    class Meta:
        model = recipe


class recipe_searchable(db.Model):
    __tablename__ = "recipe_searchable"
    recipeid = db.Column(db.Integer, primary_key=True)
    entire_recipe = db.Column(db.Text)


class recipe_searchable_schema(ma.ModelSchema):
    class Meta:
        model = recipe_searchable


class location_searchable(db.Model):
    __tablename__ = "location_searchable"
    locationid = db.Column(db.Integer, primary_key=True)
    entire_location = db.Column(db.Text)


class location_searchable_schema(ma.ModelSchema):
    class Meta:
        model = recipe_searchable


class food_searchable(db.Model):
    __tablename__ = "food_searchable"
    foodid = db.Column(db.Integer, primary_key=True)
    entire_food = db.Column(db.Text)


class food_searchable_schema(ma.ModelSchema):
    class Meta:
        model = food_searchable


class most_used_food(db.Model):
    __tablename__ = "most_used_food"
    foodid = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    count = db.Column(db.Integer, primary_key=True)


class most_used_food_schema(ma.ModelSchema):
    class Meta:
        model = most_used_food
