#!/usr/bin/env python
from flask import (
    render_template,
    send_from_directory,
    jsonify,
    Response,
    url_for,
    request,
    abort,
    Flask,
)

import json
import os
import sys
import nltk
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import and_
from flask_restful import Api, Resource, reqparse
from flask_cors import CORS
from stop_words import get_stop_words

# search query preparation
from nltk.corpus import stopwords
from nltk.stem.lancaster import LancasterStemmer
from nltk.tokenize import RegexpTokenizer

nltk.download("stopwords")
# stem the word
st = LancasterStemmer()
# remove punctuation
tokenizer = RegexpTokenizer(r"\w+")
# remove stop words
stop_words = list(get_stop_words("en"))  # About 900 stopwords
nltk_words = list(stopwords.words("english"))  # About 150 stopwords
stop_words.extend(nltk_words)


# our database models
from models import *

app = Flask(__name__)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})
api = Api(app)

# our login information
from admin_information import POSTGRES_LOGIN

app.config["SQLALCHEMY_DATABASE_URI"] = (
    "postgresql://%(user)s:%(pw)s@%(host)s:%(port)s/%(db)s" % POSTGRES_LOGIN
)
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)

@app.route("/visualization/serving_size")
def get_food_density():
    return render_template("serving_size.html")

@app.route("/visualization/food_count")
def get_food_count():
    return render_template("food_count.html")

@app.route("/visualization/states")
def get_vote_statistics():
    return render_template("states.html")

@app.route("/visualization/related_recipes")
def get_related_recipes():
    return render_template("related_recipes.html")

@app.route("/")
def index():
    return "endpoints:\n \
    /api/food/search/\n \
    /api/food/<int:foodid>\n \
    /api/recipe/search/\n \
    /api/recipe/<int:recipeid>\n \
    /api/location/search/\n \
    /api/location/<int:locationid>\n \
    /api/recipe/categories/\n \
    /api/recipe_with_food/<int:foodid>"
"""
Search Helper Functions
"""

# Parses the desired page of results for a search
def get_pg(pg):
    if not pg:
        pg = 1
    else:
        pg = int(pg)
    return pg


# Parses the desired number of results for a search
def get_number(number):
    if not number:
        number = 9
    else:
        number = int(number)
        if number < 1:
            number = 1
    return number


# Parses the desired order and attribute to order the search results by
def get_sortby(sortby, asc, possible_sorts):
    if sortby not in possible_sorts:
        sortby = possible_sorts["default"]
    else:
        sortby = possible_sorts[sortby]
    if asc is None:
        asc = True
    if asc == "False" or asc == "false":
        sortby += " desc"
    return sortby


# Refines a query by removing punctuation and stop words, stemming and
# tokenizing the remaining terms, and then ORing everything together.
def refine_query(query):
    if not query:
        query = ""
    else:
        # remove punctuation, stop words, suffix
        query = "|".join(
            [
                st.stem(word)
                for word in tokenizer.tokenize(query.lower())
                if word not in stop_words
            ]
        )
    return query


class food_search(Resource):
    """
    end point: /api/food/search/?pg=<int>              //pagination page
                                &number=<int>          //number of results on the page
                                &query=<str>           //search term
                                &sortby=<int>          //what to order results by (foodid, name, carbs, protein, fat, calories)
                                &ascending=<bool>      //if the order is ascending or not
                                &proteinmax=<int>      //max protein value
                                &proteinmin=<int>      //min protein value
                                &fatmax=<int>          //max fat value
                                &fatmin=<int>          //min fat value
                                &carbohydratemax=<int> //max carb value
                                &carbohydratemin=<int> //min carb value
                                &caloriemax=<int>      //max calorie value
                                &caloriemin=<int>      //min calorie value
    """

    def get(self):

        possible_sorts = {
            "default": "food_with_macros.foodid",
            "foodid": "food_with_macros.foodid",
            "name": "food_with_macros.name",
            "carbs": "food_with_macros.carb",
            "protein": "food_with_macros.protein",
            "fat": "food_with_macros.fat",
            "calories": "food_with_macros.calorie",
        }

        pg = get_pg(request.args.get("pg"))
        number = get_number(request.args.get("number"))
        sortby = get_sortby(
            request.args.get("sortby"), request.args.get("ascending"), possible_sorts
        )
        query = refine_query(request.args.get("query"))

        proteinmax = request.args.get("proteinmax")
        if not proteinmax:
            proteinmax = 10000.0

        proteinmin = request.args.get("proteinmin")
        if not proteinmin:
            proteinmin = 0.0

        fatmax = request.args.get("fatmax")
        if not fatmax:
            fatmax = 10000.0

        fatmin = request.args.get("fatmin")
        if not fatmin:
            fatmin = 0.0

        carbohydratemax = request.args.get("carbohydratemax")
        if not carbohydratemax:
            carbohydratemax = 10000.0

        carbohydratemin = request.args.get("carbohydratemin")
        if not carbohydratemin:
            carbohydratemin = 0.0

        caloriemax = request.args.get("caloriemax")
        if not caloriemax:
            caloriemax = 10000.0

        caloriemin = request.args.get("caloriemin")
        if not caloriemin:
            caloriemin = 0.0

        food_query_schema = food_with_macros_schema(many=True)
        nutrient_query_schema = food_nutrition_schema(many=True)

        query_foodid_result = food_searchable.query.filter(
            "entire_food similar to '%(" + query + ")%'"
        ).subquery("query_foodid_result")

        food_search_query_result = (
            food_with_macros.query.with_entities(
                food_with_macros.foodid,
                food_with_macros.name,
                food_with_macros.amount,
                food_with_macros.unit,
            )
            .filter(
                and_(
                    query_foodid_result.c.foodid == food_with_macros.foodid,
                    food_with_macros.calorie.between(caloriemin, caloriemax),
                    food_with_macros.carb.between(carbohydratemin, carbohydratemax),
                    food_with_macros.fat.between(fatmin, fatmax),
                    food_with_macros.protein.between(proteinmin, proteinmax),
                )
            )
            .order_by(sortby)
            .paginate(page=pg, per_page=number, error_out=True)
        )

        result = {"foods": []}

        for row in food_query_schema.dump(food_search_query_result.items).data:
            foodid = row["foodid"]
            nutrient_query_result = food_nutrition.query.filter_by(foodid=foodid)
            items_query_result = location_items.query.with_entities(
                location_items.locationid
            ).filter_by(foodid=foodid)

            items_query_schema = location_items_schema(many=True)

            result["foods"].append(
                {
                    "food": row,
                    "nutrients": nutrient_query_schema.dump(nutrient_query_result).data,
                    "locations": items_query_schema.dump(items_query_result).data,
                }
            )

        return jsonify(result)


class food_ID(Resource):
    """
    end point: /api/food/<int:foodid>
    """

    def get(self, foodid=None):

        items_query_schema = location_items_schema(many=True)
        name_query_schema = food_schema(many=False)
        nutrient_query_schema = food_nutrition_schema(many=True)

        name_query_result = food.query.get_or_404(foodid)
        nutrient_query_result = food_nutrition.query.filter_by(foodid=foodid)
        items_query_result = location_items.query.with_entities(
            location_items.locationid
        ).filter_by(foodid=foodid)

        return jsonify(
            {
                "food": name_query_schema.dump(name_query_result).data,
                "nutrients": nutrient_query_schema.dump(nutrient_query_result).data,
                "locations": items_query_schema.dump(items_query_result).data,
            }
        )


class recipe_search(Resource):
    """
    end point: /api/recipe/search/?pg=<int>         //pagination page
                                  &number=<int>     //number of results on page
                                  &query=<str>      //search term
                                  &sortby=<str>     //what to order results by (recipeid,name,servings,time)
                                  &ascending=<bool> //if the order by is ascending or not
                                  &category=<str>   //recipe category
                                  &servings=<int>   //serving size
                                  &mintime=<int>    //max time to cook
                                  &maxtime=<int>    //min time to cook
                                  &vegan=<bool>     //vegan
                                  &keto=<bool>      //keto
    """

    def get(self):

        possible_sorts = {
            "default": "recipe_with_categories.recipeid",
            "recipeid": "recipe_with_categories.recipeid",
            "name": "recipe_with_categories.name",
            "servings": "recipe_with_categories.recipeyield",
            "time": "recipe_with_categories.totaltime",
        }

        pg = get_pg(request.args.get("pg"))
        number = get_number(request.args.get("number"))
        sortby = get_sortby(
            request.args.get("sortby"), request.args.get("ascending"), possible_sorts
        )
        query = refine_query(request.args.get("query"))

        vegan = request.args.get("vegan")
        if not vegan:
            vegan = False
        elif vegan == "False":
            vegan = False
        else:
            vegan = True

        keto = request.args.get("keto")
        if not keto:
            keto = False
        elif keto == "False":
            keto = False
        else:
            keto = True

        category = request.args.get("category")
        if not category:
            category = ""

        servings = request.args.get("servings")
        if not servings:
            servings = "%%"

        mintime = request.args.get("mintime")
        if not mintime:
            mintime = 0

        maxtime = request.args.get("maxtime")
        if not maxtime:
            maxtime = 10000

        id_query_schema = recipe_with_categories_schema(many=True)
        name_query_schema = recipe_schema(many=False)
        category_query_schema = recipeCategory_schema(many=True)
        step_query_schema = recipeStep_schema(many=True)
        ingredient_query_schema = recipeIngredient_schema(many=True)
        recipes_query_schema = location_recipes_schema(many=True)

        query_recipeid_result = recipe_searchable.query.filter(
            "entire_recipe similar to '%(" + query + ")%'"
        ).subquery("query_recipeid_result")

        category_search_query_result = (
            recipe_with_categories.query.filter(
                query_recipeid_result.c.recipeid == recipe_with_categories.recipeid
            )
            .filter(recipe_with_categories.categories.like("%" + category + "%"))
            .filter(recipe_with_categories.vegan == vegan)
            .filter(recipe_with_categories.keto == keto)
            .filter(recipe_with_categories.recipeyield.like(servings))
            .filter(recipe_with_categories.totaltime.between(mintime, maxtime))
            .order_by(sortby)
            .paginate(page=pg, per_page=number, error_out=True)
        )

        result = {"recipes": []}

        for row in id_query_schema.dump(category_search_query_result.items).data:
            recipeid = row["recipeid"]
            name_query_result = recipe.query.get_or_404(recipeid)
            category_query_result = recipeCategory.query.filter_by(
                recipeid=recipeid
            ).with_entities(recipeCategory.category)
            step_query_result = recipeStep.query.filter_by(recipeid=recipeid)
            ingredient_query_result = recipeIngredient.query.filter_by(
                recipeid=recipeid
            )
            recipes_query_result = location_recipes.query.with_entities(
                location_recipes.locationid
            ).filter_by(recipeid=recipeid)

            result["recipes"].append(
                {
                    "recipe": name_query_schema.dump(name_query_result).data,
                    "category": category_query_schema.dump(category_query_result).data,
                    "steps": step_query_schema.dump(step_query_result).data,
                    "ingredients": ingredient_query_schema.dump(
                        ingredient_query_result
                    ).data,
                    "locations": recipes_query_schema.dump(recipes_query_result).data,
                }
            )

        return jsonify(result)


class recipe_ID(Resource):
    """
    end point : /api/recipe/<int:recipeid>
    """

    def get(self, recipeid=None):

        name_query_schema = recipe_schema(many=False)
        category_query_schema = recipeCategory_schema(many=True)
        step_query_schema = recipeStep_schema(many=True)
        ingredient_query_schema = recipeIngredient_schema(many=True)
        recipes_query_schema = location_recipes_schema(many=True)

        name_query_result = recipe.query.get_or_404(recipeid)
        category_query_result = recipeCategory.query.filter_by(recipeid=recipeid)
        step_query_result = recipeStep.query.filter_by(recipeid=recipeid)
        ingredient_query_result = recipeIngredient.query.filter_by(recipeid=recipeid)
        recipes_query_result = location_recipes.query.with_entities(
            location_recipes.locationid
        ).filter_by(recipeid=recipeid)

        return jsonify(
            {
                "recipe": name_query_schema.dump(name_query_result).data,
                "category": category_query_schema.dump(category_query_result).data,
                "steps": step_query_schema.dump(step_query_result).data,
                "ingredients": ingredient_query_schema.dump(
                    ingredient_query_result
                ).data,
                "locations": recipes_query_schema.dump(recipes_query_result).data,
            }
        )


class recipe_categories(Resource):
    """
    end point : /api/recipe/categories/
    """

    def get(self):

        category_query_schema = recipeCategory_schema(many=True)

        category_query_result = (
            recipeCategory.query.with_entities(recipeCategory.category)
            .distinct(recipeCategory.category)
            .all()
        )

        return jsonify(
            {"result": category_query_schema.dump(category_query_result).data}
        )


class location_search(Resource):
    """
    end point /api/location/search/?pg=<int>         //pagination page
                                   &number=<int>     //number of results on the page
                                   &query=<str>      //search term
                                   &sortby=<str>     //what to order results by (locationid, name, rating, price)
                                   &ascending=<bool> //if the order is ascending or not
                                   &rating=<int>     //the rating of the location
                                   &storetype=<str>  //the type of location
                                   &price=<int>      //the price bucket of the location
    """

    def get(self):

        arguments = request.args.to_dict()
        possible_sorts = {
            "default": "locations.locationid",
            "locationid": "locations.locationdid",
            "name": "locations.name",
            "rating": "locations.rating",
            "price": "locations.rating",
        }

        pg = get_pg(request.args.get("pg"))
        number = get_number(request.args.get("number"))
        sortby = get_sortby(
            request.args.get("sortby"), request.args.get("ascending"), possible_sorts
        )
        query = refine_query(request.args.get("query"))

        rating = request.args.get("rating")
        if not rating:
            rating = 0
        else:
            rating = int(rating)

        price = request.args.get("price")
        if not price:
            price = 5
        else:
            price = int(price)

        storetype = request.args.get("storetype")
        if not storetype:
            storetype = "%%"

        name_query_schema = location_schema(many=True)
        items_query_schema = location_items_schema(many=True)
        recipes_query_schema = location_recipes_schema(many=True)

        query_locationid_result = location_searchable.query.filter(
            "entire_location similar to '%(" + query + ")%'"
        ).subquery("query_locationid_result")

        name_query_result = (
            locations.query.filter(
                query_locationid_result.c.locationid == locations.locationid
            )
            .filter(locations.rating >= rating)
            .filter(locations.price <= price)
            .filter(locations.storetype.like(storetype))
            .order_by(sortby)
            .paginate(page=pg, per_page=number, error_out=True)
        )

        result = {"locations": []}

        for row in name_query_schema.dump(name_query_result.items).data:
            locationid = row["locationid"]
            items_query_result = location_items.query.with_entities(
                location_items.foodid
            ).filter_by(locationid=locationid)
            recipes_query_result = location_recipes.query.with_entities(
                location_recipes.recipeid
            ).filter_by(locationid=locationid)

            result["locations"].append(
                {
                    "location": row,
                    "items": items_query_schema.dump(items_query_result).data,
                    "recipes": recipes_query_schema.dump(recipes_query_result).data,
                }
            )

        return jsonify(result)


class location_ID(Resource):
    """
    end point: /api/location/<int:locationid>
    """

    def get(self, locationid=None):

        name_query_schema = location_schema(many=False)
        items_query_schema = location_items_schema(many=True)
        recipes_query_schema = location_recipes_schema(many=True)

        name_query_result = locations.query.get_or_404(locationid)
        items_query_result = location_items.query.with_entities(
            location_items.foodid
        ).filter_by(locationid=locationid)
        recipes_query_result = location_recipes.query.with_entities(
            location_recipes.recipeid
        ).filter_by(locationid=locationid)

        return jsonify(
            {
                "location": name_query_schema.dump(name_query_result).data,
                "items": items_query_schema.dump(items_query_result).data,
                "recipes": recipes_query_schema.dump(recipes_query_result).data,
            }
        )


class recipe_with_food(Resource):
    """
    end point: /api/location/recipe_with_food/<int:foodid>
    """

    def get(self, foodid=None):

        name_query_schema = recipe_schema(many=True)

        name_query_result = recipe.query.join(
            recipeIngredient, recipeIngredient.recipeid == recipe.recipeid
        ).filter_by(foodid=foodid)

        return jsonify({"recipe": name_query_schema.dump(name_query_result).data})

class food_count(Resource):
    """
    end point: /api/visualization/food_count/
    """

    def get(self):
        count_query_schema = most_used_food_schema(many=True)
        count_query = most_used_food.query.all()

        return jsonify({"result": count_query_schema.dump(count_query).data})

api.add_resource(food_search,       "/api/food/search/", methods=["GET"])
api.add_resource(food_ID,           "/api/food/<int:foodid>", methods=["GET"])
api.add_resource(recipe_search,     "/api/recipe/search/", methods=["GET"])
api.add_resource(recipe_ID,         "/api/recipe/<int:recipeid>", methods=["GET"])
api.add_resource(location_search,   "/api/location/search/", methods=["GET"])
api.add_resource(location_ID,       "/api/location/<int:locationid>", methods=["GET"])
api.add_resource(recipe_categories, "/api/recipe/categories/", methods=["GET"])
api.add_resource(
    recipe_with_food,               "/api/recipe_with_food/<int:foodid>", methods=["GET"]
)
api.add_resource(food_count, "/api/visualizations/food_count/", methods=["GET"])

if __name__ == "__main__":
    if os.environ.get("PROD") == "1":
        app.jinja_env.cache = {}
        app.run(host="0.0.0.0", debug=False)
    else:
        app.run(
            host="0.0.0.0",
            port=3000,
            debug=True,
            ssl_context=("fullchain.pem", "privkey.pem"),
        )
