import requests
import json
import unittest
import os
from main import refine_query

api_url = "https://unbeatable-food.com:3000/"


class UnitTest(unittest.TestCase):
    """
    Unit Tests: main.py
    """

    # Tests that the API is up
    def test_root(self):
        response = requests.get(api_url)
        self.assertEqual(response.status_code, 200)

    # Tests a valid foodid
    def test_food_id_1(self):
        response = requests.get(api_url + "api/food/1001")
        file = open("test_json/foodid1001.json")
        expected = json.load(file)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected)
        file.close()

    # Tests an invalid foodid
    def test_food_id_none(self):
        response = requests.get(api_url + "api/food/1")
        self.assertEqual(response.status_code, 404)

    # Tests the page filter of foods
    def test_food_pg(self):
        response = requests.get(api_url + "api/food/search/")
        expected = requests.get(api_url + "api/food/search/?pg=1")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected.json())

    # Tests the caloriemin filter of foods
    def test_food_caloriemin(self):
        response = requests.get(api_url + "api/food/search/?caloriemin=885")
        expected = {"foods": []}
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected)

    # Tests the caloriemax filter of foods
    def test_food_caloriemax(self):
        response = requests.get(api_url + "api/food/search/?caloriemax=885")
        expected = requests.get(api_url + "api/food/search/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected.json())

    # Tests the fatmin filter of foods
    def test_food_fatmin(self):
        response = requests.get(api_url + "api/food/search/?fatmin=101")
        expected = {"foods": []}
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected)

    # Tests the fatmax filter of foods
    def test_food_fatmax(self):
        response = requests.get(api_url + "api/food/search/?fatmax=101")
        expected = requests.get(api_url + "api/food/search/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected.json())

    # Tests the carbmin filter of foods
    def test_food_carbmin(self):
        response = requests.get(api_url + "api/food/search/?carbohydratemin=108")
        expected = {"foods": []}
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected)

    # Tests the carbmax filter of foods
    def test_food_carbmax(self):
        response = requests.get(api_url + "api/food/search/?carbohydratemax=108")
        expected = requests.get(api_url + "api/food/search/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected.json())

    # Tests the proteinmin filter of foods
    def test_food_proteinmin(self):
        response = requests.get(api_url + "api/food/search/?proteinmin=86")
        expected = {"foods": []}
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected)

    # Tests the proteinmax filter of foods
    def test_food_proteinmax(self):
        response = requests.get(api_url + "api/food/search/?proteinmax=86")
        expected = requests.get(api_url + "api/food/search/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected.json())

    # Tests searching of foods
    def test_food_search(self):
        response = requests.get(api_url + "api/food/search/?query=bean")
        file = open("test_json/beanfood.json")
        expected = json.load(file)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected)
        file.close()

    # Tests an invalid page
    def test_food_pg_none(self):
        response = requests.get(api_url + "api/food/search/?pg=100")
        self.assertEqual(response.status_code, 404)

    # Tests a valid recipeid
    def test_recipe_id_1(self):
        response = requests.get(api_url + "api/recipe/211913")
        file = open("test_json/recipeid211913.json")
        expected = json.load(file)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected)
        file.close()

    # Tests an invalid recipeid
    def test_recipe_id_none(self):
        response = requests.get(api_url + "api/recipe/1")
        self.assertEqual(response.status_code, 404)

    # Tests the page filter of recipes
    def test_recipe_pg(self):
        response = requests.get(api_url + "api/recipe/search/")
        expected = requests.get(api_url + "api/recipe/search/?pg=1")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected.json())

    # Tests the category filter of recipes
    def test_recipe_category_bad(self):
        response = requests.get(api_url + "api/recipe/search/?category=fake")
        expected = {"recipes": []}
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected)

    # Tests the servings filter of recipes
    def test_recipe_servings(self):
        response = requests.get(api_url + "api/recipe/search/?servings=97")
        expected = {"recipes": []}
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected)

    # Tests the mintime filter of recipes
    def test_recipe_mintime(self):
        response = requests.get(api_url + "api/recipe/search/?mintime=1441")
        expected = {"recipes": []}
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected)

    # Tests the maxtime filter of foods
    def test_recipe_maxtime(self):
        response = requests.get(api_url + "api/food/search/?maxtime=1441")
        expected = requests.get(api_url + "api/food/search/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected.json())

    # Tests searching of recipes
    def test_recipe_search(self):
        response = requests.get(api_url + "api/recipe/search/?query=bean")
        file = open("test_json/beanrecipe.json")
        expected = json.load(file)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected)
        file.close()

    # Tests an invalid page
    def test_recipe_pg_none(self):
        response = requests.get(api_url + "api/recipe/search/?pg=100")
        self.assertEqual(response.status_code, 404)

    # Tests a valid locationid
    def test_location_id_1(self):
        response = requests.get(api_url + "api/location/1")
        file = open("test_json/locationid1.json")
        expected = json.load(file)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected)
        file.close()

    # Tests an invalid locationid
    def test_location_id_none(self):
        response = requests.get(api_url + "api/location/65")
        self.assertEqual(response.status_code, 404)

    # Tests the page filter of locations
    def test_location_pg(self):
        response = requests.get(api_url + "api/location/search/")
        expected = requests.get(api_url + "api/location/search/?pg=1")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected.json())

    # Tests the store type filter of locations
    def test_location_type_bad(self):
        response = requests.get(api_url + "api/location/search/?storetype=fake")
        expected = {"locations": []}
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected)

    # Tests the price filter of locations
    def test_location_price_none(self):
        response = requests.get(api_url + "api/location/search?price=0")
        expected = {"locations": []}
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected)

    # Tests the price filter of locations
    def test_location_price_all(self):
        response = requests.get(api_url + "api/location/search?price=3")
        expected = requests.get(api_url + "api/location/search/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected.json())

    # Tests the rating filter of locations
    def test_location_rating_none(self):
        response = requests.get(api_url + "api/location/search?rating=6")
        expected = {"locations": []}
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected)

    # Tests the rating filter of locations
    def test_location_rating_all(self):
        response = requests.get(api_url + "api/location/search?rating=0")
        expected = requests.get(api_url + "api/location/search/")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected.json())

    # Tests searching of locations
    def test_location_search(self):
        response = requests.get(api_url + "api/location/search/?query=bean")
        file = open("test_json/beanlocation.json")
        expected = json.load(file)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), expected)
        file.close()

    # Tests an invalid page
    def test_location_pg_none(self):
        response = requests.get(api_url + "api/location/search/?pg=100")
        self.assertEqual(response.status_code, 404)

    # Tests the query refining function
    def test_refine_query1(self):
        result = refine_query("it it it it it it it BEANED!!!!!!!!!")
        self.assertEqual(result, "bean")

    def test_refine_query2(self):
        result = refine_query("beaned beans beaning")
        self.assertEqual(result, "bean|bean|bean")

    def test_refine_query3(self):
        result = refine_query("it it it the and and if")
        self.assertEqual(result, "")

    def test_refine_query3(self):
        result = refine_query('bean !!!!! bean\'s ^&? $%beAnEd"""')
        self.assertEqual(result, "bean|bean|bean")


if __name__ == "__main__":
    unittest.main()
