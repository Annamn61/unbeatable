var assert = require("chai").assert;
var recipeQuery = require("../../../src/queries/recipeQuery")
var foodQuery = require("../../../src/queries/foodQuery")
var locationQuery = require("../../../src/queries/locationQuery")
const fetch = require("node-fetch");

describe('Recipe Query', () => {
    it('Recipe ID 211913 should return steak and kidney pie', async () => {
        await fetch('https://52.203.124.215:5000/api/recipe/211913')
            .then((res) => {
                return res.json()
            })
            .then((res) => {
                //console.log(res);
                assert.equal(res["recipe"]["name"], "The ultimate makeover: Steak & kidney pie")
            })
    })
})

describe('Recipe Query', () => {
    it('Recipe ID 559227 should return Steak and Salad Tacos', async () => {
        await fetch('https://52.203.124.215:5000/api/recipe/559227')
            .then((res) => {
                return res.json()
            })
            .then((res) => {
                //console.log(res);
                assert.equal(res["recipe"]["name"], "Steak and Salad Tacos")
            })
    })
})

describe('Recipe Query', () => {
    it('Recipe ID 550021 should return Steak and French Fry Salad with Blue Cheese Butter + Poached Eggs', async () => {
        await fetch('https://52.203.124.215:5000/api/recipe/550021')
            .then((res) => {
                return res.json()
            })
            .then((res) => {
                //console.log(res);
                assert.equal(res["recipe"]["name"], "Steak and French Fry Salad with Blue Cheese Butter + Poached Eggs")
            })
    })
})

describe('Food Query', () => {
    it('Food ID 1004 should return blue cheese', async () => {
        await fetch('https://52.203.124.215:5000/api/food/1004')
            .then((res) => {
                return res.json()
            })
            .then((res) => {
                //console.log(res);
                assert.equal(res["food"]["name"], "blue cheese")
            })
    })
})

describe('Food Query', () => {
    it('Food ID 1001 should return butter', async () => {
        await fetch('https://52.203.124.215:5000/api/food/1001')
            .then((res) => {
                return res.json()
            })
            .then((res) => {
                //console.log(res);
                assert.equal(res["food"]["name"], "butter")
            })
    })
})

describe('Food Query', () => {
    it('Food ID 1026 should return mozzarella', async () => {
        await fetch('https://52.203.124.215:5000/api/food/1026')
            .then((res) => {
                return res.json()
            })
            .then((res) => {
                //console.log(res);
                assert.equal(res["food"]["name"], "mozzarella")
            })
    })
})

describe('Food Query', () => {
    it('Food ID 1033 should return parmesan cheese', async () => {
        await fetch('https://52.203.124.215:5000/api/food/1033')
            .then((res) => {
                return res.json()
            })
            .then((res) => {
                //console.log(res);
                assert.equal(res["food"]["name"], "parmesan cheese")
            })
    })
})

describe('Location Query', () => {
    it('Location ID 1 should return HEB 41st Street', async () => {
        await fetch('https://52.203.124.215:5000/api/location/1')
            .then((res) => {
                return res.json()
            })
            .then((res) => {
                //console.log(res);
                assert.equal(res["location"]["name"], "HEB 41st Street")
            })
    })
})

describe('Location Query', () => {
    it('Location ID 2 should return Wheatsville Food Co-op', async () => {
        await fetch('https://52.203.124.215:5000/api/location/2')
            .then((res) => {
                return res.json()
            })
            .then((res) => {
                //console.log(res);
                assert.equal(res["location"]["name"], "Wheatsville Food Co-op")
            })
    })
})

describe('Location Query', () => {
    it('Location ID 3 should return Fresh Plus Grocery', async () => {
        await fetch('https://52.203.124.215:5000/api/location/3')
            .then((res) => {
                return res.json()
            })
            .then((res) => {
                //console.log(res);
                assert.equal(res["location"]["name"], "Fresh Plus Grocery")
            })
    })
})

//Phase 3 Tests
//These test new API endpoints that we used to accomodate searching, filtering, and sorting

//Test 1: sorting recipes by name, descending

describe('Recipe Sort Query', () => {
    it('The last recipe alphabetically should be Whole Grain Strawberry Breakfast Bars + VIDEO', async () => {
        await fetch('https://www.unbeatable-food.com:5000/api/recipe/search/?pg=1&sortby=name&ascending=false')
            .then((res) => {
                return res.json()
            })
            .then((res) => {
                //console.log(res);
                assert.equal(res["recipes"][0]["recipe"]["name"], "Whole Grain Strawberry Breakfast Bars + VIDEO")
            })
    })
})

//Test 2: filtering recipes by max time

describe('Recipe Filter Query', () => {
    it('The first recipe when filtering between a min time of 10min and a max time of 30 min should be Grilled Steak Sandwiches With Goat Cheese And Arugula', async () => {
        await fetch('https://www.unbeatable-food.com:5000/api/recipe/search/?pg=1&maxtime=30&mintime=10')
            .then((res) => {
                return res.json()
            })
            .then((res) => {
                //console.log(res);
                assert.equal(res["recipes"][0]["recipe"]["name"], "Grilled Steak Sandwiches With Goat Cheese And Arugula")
            })
    })
})

//Test 3: sorting and filtering recipes by time

describe('Recipe Sort and Filter Query', () => {
    it('The first recipe when filtering by time between 50 and 60 min and sorting by ascending time should be Black Bean Cornmeal Pie', async () => {
        await fetch('https://www.unbeatable-food.com:5000/api/recipe/search/?pg=1&maxtime=60&mintime=50&sortby=time&ascending=true')
            .then((res) => {
                return res.json()
            })
            .then((res) => {
                //console.log(res);
                assert.equal(res["recipes"][0]["recipe"]["name"], "Black Bean Cornmeal Pie")
            })
    })
})

//Test 4: sorting foods by protein, descending

describe('Nutrition Sort Query', () => {
    it('The first ingredient when sorting by descending protein should be gelatine', async () => {
        await fetch('https://www.unbeatable-food.com:5000/api/food/search/?pg=1&sortby=protein&ascending=false')
            .then((res) => {
                return res.json()
            })
            .then((res) => {
                //console.log(res);
                assert.equal(res["foods"][0]["food"]["name"], "gelatine")
            })
    })
})

//Test 5: filtering foods by calories

describe('Nutrition Filter Query', () => {
    it('The first ingredient when filtering calories between 0 and 50 should be low fat milk', async () => {
        await fetch('https://www.unbeatable-food.com:5000/api/food/search/?pg=1&caloriemin=0&caloriemax=50')
            .then((res) => {
                return res.json()
            })
            .then((res) => {
                //console.log(res);
                assert.equal(res["foods"][0]["food"]["name"], "low fat milk")
            })
    })
})

//Test 6: sorting and filtering recipes by time

describe('Nutrition Sort and Filter Query', () => {
    it('The first ingredient when sorting and filtering by fat between 10 and 30 min should be graham cracker crumbs', async () => {
        await fetch('https://www.unbeatable-food.com:5000/api/food/search/?pg=1&fatmin=10&fatmax=30&sortby=fat&ascending=true')
            .then((res) => {
                return res.json()
            })
            .then((res) => {
                //console.log(res);
                assert.equal(res["foods"][0]["food"]["name"], "graham cracker crumbs")
            })
    })
})

//Test 7: sorting locations by price, ascending

describe('Location Sort Query', () => {
    it('The first location when sorting by ascending price should be Ctown Supermarkets', async () => {
        await fetch('https://www.unbeatable-food.com:5000/api/location/search/?pg=1&sortby=price&ascending=true')
            .then((res) => {
                return res.json()
            })
            .then((res) => {
                //console.log(res);
                assert.equal(res["locations"][0]["location"]["name"], "Ctown Supermarkets")
            })
    })
})

//Test 8: filtering locations by storeType

describe('Location Filter Query', () => {
    it('The first location when filtering by grocery stores should be Wheatsville Food Co-op', async () => {
        await fetch('https://www.unbeatable-food.com:5000/api/location/search/?pg=1&storetype=Grocery')
            .then((res) => {
                return res.json()
            })
            .then((res) => {
                //console.log(res);
                assert.equal(res["locations"][0]["location"]["name"], "Wheatsville Food Co-op")
            })
    })
})

//Test 9: sorting and filtering locations by price, descending

describe('Location Sort and Filter Query', () => {
    it('The first location when sorting and filtering by a price of 3 should be Natural Grocers', async () => {
        await fetch('https://www.unbeatable-food.com:5000/api/location/search/?pg=1&sortby=price&ascending=false&price=3')
            .then((res) => {
                return res.json()
            })
            .then((res) => {
                //console.log(res);
                assert.equal(res["locations"][0]["location"]["name"], "Natural Grocers ")
            })
    })
})

//Test 10: filtering recipes by diet

describe('Recipe Filter Query 2', () => {
    it('The first recipe when filtering by vegan should be Drinking Seasonally: Sour Cherry Caipirinha', async () => {
        await fetch('https://www.unbeatable-food.com:5000/api/recipe/search/?pg=1&vegan=true')
            .then((res) => {
                return res.json()
            })
            .then((res) => {
                //console.log(res);
                assert.equal(res["recipes"][0]["recipe"]["name"], "Drinking Seasonally: Sour Cherry Caipirinha")
            })
    })
})