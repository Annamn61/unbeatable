from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

def navTo(driver, navPage):
    element = driver.find_element_by_id(navPage + "_nav")
    element.click()

def navTest(driver, webUrl, elem, appendix):
    driver.get(webUrl)
    navTo(driver, elem)
    expected = webUrl + "/" + appendix
    success = driver.current_url == expected
    return success

def recipeInstancePageTest(driver, webUrl):
    driver.get(webUrl)
    driver.find_element_by_id("recipe_nav").click()
    """expected element attribute
    success = 
    """
    success = false
    return success

def refreshTest(driver, webUrl, navPage, elemID):
    driver.get(webUrl)
    navTo(driver, navPage)
    driver.refresh()
    success = False
    try:
        driver.find_element_by_id(elemID)
    except:
        pass
    else:
        success = True
    return success


def testPrint(success, testNo):
    if(success):
        print("Passed test " + str(testNo))
    else:
        print("Test " + str(testNo) + " FAILED")


def landingComponentTest(driver, webUrl, elemID, expectedUrl):
    time.sleep(1)
    driver.get(webUrl)
    success = False
    time.sleep(1)
    driver.find_element_by_id(elemID).click()
    time.sleep(1)
    success = expectedUrl in driver.current_url
    return success

def filterTest():
    time.sleep(1)

def searchTest(driver, webUrl, elemID, searchText, searchButton):
    driver.get(webUrl)
    driver.find_element_by_id(elemID).send_keys(searchText)
    driver.find_element_by_id(searchButton).click()
    time.sleep(1)
    result = driver.find_element_by_id("title_text").text
    success = searchText.upper() in result
    return success


websiteUrl = "http://localhost:3000"
driver = webdriver.Chrome()




print("Testing the Navigation Bar")
testPrint(navTest(driver, websiteUrl, "about", "about"),1)
testPrint(navTest(driver, websiteUrl, "recipe", "RecipeLand"),2)
testPrint(navTest(driver, websiteUrl, "home", "home"),3)
testPrint(navTest(driver, websiteUrl, "nutrition", "NutritionLand"),4)
testPrint(navTest(driver, websiteUrl, "location", "LocationLand"),5)

print("\nTesting page refreshing")
testPrint(refreshTest(driver, websiteUrl, "about", "about_landing"),6)
testPrint(refreshTest(driver, websiteUrl, "recipe", "recipe_landing"),7)
testPrint(refreshTest(driver, websiteUrl, "home", "home_landing"),8)
testPrint(refreshTest(driver, websiteUrl, "nutrition", "nutrition_landing"),9)
testPrint(refreshTest(driver, websiteUrl, "location", "location_landing"),10)

print("\nTesting that the search box returns relevant results")
time.sleep(1)
testPrint(searchTest(driver, websiteUrl+"/NutritionLand", "search_nutrition", "bean", "search_button"), 11)
testPrint(searchTest(driver, websiteUrl+"/NutritionLand", "search_nutrition", "steak", "search_button"), 12)
testPrint(searchTest(driver, websiteUrl+"/NutritionLand", "search_nutrition", "cake", "search_button"), 13)
time.sleep(1)
testPrint(searchTest(driver, websiteUrl+"/RecipeLand", "search_recipe", "Steak", "search_button"), 14)
testPrint(searchTest(driver, websiteUrl+"/RecipeLand", "search_recipe", "pizza", "search_button"), 15)
testPrint(searchTest(driver, websiteUrl+"/RecipeLand", "search_recipe", "breakfast", "search_button"), 16)
time.sleep(1)
testPrint(searchTest(driver, websiteUrl+"/LocationLand", "search_location", "heb", "search_button"), 17)
testPrint(searchTest(driver, websiteUrl+"/LocationLand", "search_location", "market", "search_button"), 18)
testPrint(searchTest(driver, websiteUrl+"/LocationLand", "search_location", "whole foods", "search_button"), 19)
time.sleep(1)

print("\nTesting that landing page components navigate correctly")
time.sleep(1)
testPrint(landingComponentTest(driver, websiteUrl + "/recipes/203511", "click" ,"nutrition_facts"), 20)
time.sleep(1)
testPrint(landingComponentTest(driver, websiteUrl + "/nutrition_facts/1036", "click" , "recipes"), 21)
time.sleep(1)

testPrint(landingComponentTest(driver, websiteUrl + "/locations/5", "click" ,"nutrition_facts"), 22)

print("\nTesting global search")
testPrint(navTest(driver, websiteUrl, "search", "search/a"),23)

# testPrint(searchTest(driver, websiteUrl, "nav_search_id", "bean", "everything_search"), 11)



#print("\nTesting that the search box returns relevant results")
#time.sleep(1)




driver.quit()

