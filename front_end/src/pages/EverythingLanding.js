import React, { Component } from "react";
import "../index.css";
//import PaginationElement from "../components/PaginationElement";
import LocationLandingComponent from "../components/LocationLandingComponent";
import NutritionLandingComponent from "../components/NutritionLandingComponent";
import RecipeLandingComponent from "../components/RecipeLandingComponent";

class EverythingLanding extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: this.props.match.params.searchTerm,
      comps: [],
      locations: [],
      recipeComps: [],
      recipes: [],
      foodComps: [],
      foodItems: []
    };
  }

  async componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      await this.setState({
        searchTerm: nextProps.match.params.searchTerm
      });
      await this.setState(nextProps);
      this.updatePage();
    }
  }

  componentDidMount() {
    this.updatePage();
  }

  updatePage() {
    this.setLocations();
    this.setRecipes();
    this.setNutrition();
    this.updateComps();
  }

  setRecipes() {
    let url =
      "http://www.unbeatable-food.com:5000/api/recipe/search/?pg=" +
      "&number=3" +
      "&query=" +
      this.state.searchTerm;

    fetch(url)
      .then(res => res.json())
      .then(data => {
        let recipes = [];
        //for each recipe
        for (let item in data) {
          for (let index in data[item]) {
            let thisRecipe = data[item][index];
            //loop through the categories
            let recipeCategories = [];
            for (let category in thisRecipe.category) {
              recipeCategories.push(thisRecipe.category[category].category);
            }
            //loop through the steps!
            let recipeSteps = [];
            for (let step in thisRecipe.steps) {
              recipeSteps.push({
                description: thisRecipe.steps[step].description,
                step: thisRecipe.steps[step].step
              });
            }

            //loop through the ingredients!
            let recipeIngredients = [];
            for (let ingredient in thisRecipe.ingredients) {
              recipeIngredients.push({
                amount: thisRecipe.ingredients[ingredient].amount,
                unit: thisRecipe.ingredients[ingredient].unit,
                foodid: thisRecipe.ingredients[ingredient].foodid
              });
            }

            if (thisRecipe != undefined) {
              recipes.push({
                name: thisRecipe.recipe.name,
                author: thisRecipe.recipe.author,
                authorabout: thisRecipe.recipe.authorabout,
                description: thisRecipe.recipe.description,
                imageurl: thisRecipe.recipe.imageurl,
                keto: thisRecipe.recipe.keto,
                recipeid: thisRecipe.recipe.recipeid,
                recipeyield: thisRecipe.recipe.recipeyield,
                sustainable: thisRecipe.recipe.sustainable,
                totaltime: thisRecipe.recipe.totaltime,
                vegan: thisRecipe.recipe.vegan,
                steps: recipeSteps,
                categories: recipeCategories,
                ingredients: recipeIngredients
              });
            }
          }
        }

        this.setState(
          {
            recipes: recipes
          },
          () => this.updateComps()
        );
      })
      .catch(error => {
        console.log(error);
        let recipes = [];
        this.setState(
          {
            recipes: recipes
          },
          () => this.updateComps()
        );
      });
  }

  setNutrition() {
    let url =
      "http://www.unbeatable-food.com:5000/api/food/search/?pg=" +
      "&number=3" +
      "&query=" +
      this.state.searchTerm;

    fetch(url)
      .then(res => res.json())
      .then(data => {
        let foodItems = [];
        //for each differnet food item
        for (let item in data) {
          //for the nutrients in each food item
          for (let index in data[item]) {
            let totalNutrients = [];

            //for each individual nutrient in the food item
            for (let nutrient in data[item][index]["nutrients"]) {
              let nutrientItem = data[item][index]["nutrients"][nutrient];
              let singleNutrient = [];
              singleNutrient.push({
                name: nutrientItem.name,
                amount: nutrientItem.amount,
                unit: nutrientItem.unit
              });
              //add to this food items list
              totalNutrients.push({
                singleNutrient: singleNutrient
              });
            }

            //all information about
            foodItems.push({
              name: data[item][index]["food"].name,
              amount: data[item][index]["food"].amount,
              unit: data[item][index]["food"].unit,
              foodid: data[item][index]["food"].foodid,
              totalNutrients: totalNutrients
            });
          }
        }

        this.setState(
          {
            foodItems: foodItems
          },
          () => this.updateComps()
        );
      })
      .catch(error => {
        console.log(error);
        let foodItems = [];
        this.setState(
          {
            foodItems: foodItems
          },
          () => this.updateComps()
        );
      });
  }

  setLocations() {
    let url =
      "http://www.unbeatable-food.com:5000/api/location/search/?pg=" +
      "&number=3" +
      "&query=" +
      this.state.searchTerm;

    fetch(url)
      .then(res => res.json())
      .then(data => {
        let locations = [];
        // let newState = [];
        for (let item in data) {
          for (let index in data[item]) {
            let thisLocation = data[item][index];
            //for items
            let locationFoods = [];
            for (let food in thisLocation.items) {
              locationFoods.push(thisLocation.items[food].foodid);
            }

            locations.push({
              name: thisLocation.location.name,
              address: thisLocation.location.address,
              description: thisLocation.location.description,
              image: thisLocation.location.image,
              number: thisLocation.location.number,
              price: thisLocation.location.price,
              rating: thisLocation.location.rating,
              storetype: thisLocation.location.storetype,
              locationFoods: locationFoods
            });
          }
        }

        this.setState(
          {
            locations: locations
          },
          () => this.updateComps()
        );
      })
      .catch(error => {
        console.log(error);
        let recipes = [];
        this.setState(
          {
            recipes: recipes
          },
          () => this.updateComps()
        );
      });
  }

  updateComps() {
    //for locations
    let comps = this.state.locations.map(item => (
      <LocationLandingComponent
        id={item.nbd}
        item={item}
        highlighted={this.state.searchTerm}
      />
    ));

    let foodComps = this.state.foodItems.map(item => (
      //   //can this just pass in the item and then be parsed in the component?
      <NutritionLandingComponent
        id={item.foodid}
        item={item}
        highlighted={this.state.searchTerm}
      />
    ));

    let recipeComps = this.state.recipes.map(item => (
      //can this just pass in the item and then be parsed in the component?
      <RecipeLandingComponent
        highlighted={this.state.searchTerm}
        id={item.foodid}
        item={item}
      />
    ));

    this.setState({
      comps: comps,
      recipeComps: recipeComps,
      foodComps: foodComps
    });
  }

  render() {
    return (
      <div id="everything_landing" className="landingPage">
        <div>
          <div>
            <h1 className="display-3">Recipes</h1>
            <p className="blockquote">
              Find the perfect meal for any occassion.
            </p>
            <div className="landingPageGrid">
              <div className="card-columns">{this.state.recipeComps}</div>
            </div>
            <h1 className="display-3">Food</h1>
            <p className="blockquote">
              Maximize your health by finding out exactly what you are putting
              in your body.
            </p>
            <div className="landingPageGrid" id="title_text">
              <div className="card-columns">{this.state.foodComps}</div>
            </div>
            <h1 className="display-3">Locations</h1>
            <p className="blockquote">Find where to get food near you.</p>
            <div className="landingPageGrid">
              <div className="card-columns">{this.state.comps}</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default EverythingLanding;
