import React, { Component } from "react";
import green from "../assets/GreenIngredients.jpg";
import "../index.css";
import PaginationElement from "../components/PaginationElement";
import NutritionLandingComponent from "../components/NutritionLandingComponent";
import NutritionFilterBar from "../components/NutritionFilterBar";

class NutritionLanding extends Component {
  constructor(props) {
    super(props);

    this.state = {
      nbd_numbers: [],
      currPage: 1,
      list: [],
      sortBy: "none",
      ascending: true,
      userSearch: "",
      calValue: { min: 0, max: 500 },
      fatValue: { min: 0, max: 20 },
      carbValue: { min: 0, max: 100 },
      proteinValue: { min: 0, max: 20 },
      foodItems: [],
      comps: [],
      query: ""
    };

    this.pageUpdate = this.pageUpdate.bind(this);
    this.sortFunc = this.sortFunc.bind(this);
    this.calFunc = this.calFunc.bind(this);
    this.fatFunc = this.fatFunc.bind(this);
    this.carbFunc = this.carbFunc.bind(this);
    this.proteinFunc = this.proteinFunc.bind(this);
    this.searchFunc = this.searchFunc.bind(this);
  }

  async componentDidMount() {
    await this.nbdUpdate(this.state.currPage);
    this.updateComps();
  }

  sortFunc(sortBy) {
    let sortId = "";
    let ascending = true;
    //let ids = ["foodid", "name", "carbs", "protein", "fat", "calories"];
    switch (sortBy) {
      case "Name A-Z":
        sortId = "name";
        break;

      case "Name Z-A":
        sortId = "name";
        ascending = false;
        break;

      case "Calories Low-High":
        sortId = "calories";
        break;

      case "Calories High-Low":
        sortId = "calories";
        ascending = false;
        break;

      case "Protein Low-High":
        sortId = "protein";
        break;

      case "Protein High-Low":
        sortId = "protein";
        ascending = false;
        break;
    }
    this.setState(
      {
        sortBy: sortId,
        ascending: ascending
      },
      () => this.update()
    );
  }

  calFunc(calValue) {
    this.setState(
      {
        calValue: calValue
      },
      () => this.update()
    );
  }

  fatFunc(fatValue) {
    this.setState(
      {
        fatValue: fatValue
      },
      () => this.update()
    );
  }

  carbFunc(carbValue) {
    this.setState(
      {
        carbValue: carbValue
      },
      () => this.update()
    );
  }

  proteinFunc(proteinValue) {
    this.setState(
      {
        proteinValue: proteinValue
      },
      () => this.update()
    );
  }

  async update() {
    await this.nbdUpdate(this.state.currPage);
    this.updateComps();
  }

  searchFunc(query) {
    console.log("----search func" + query);
    this.setState(
      {
        query: query
      },
      () => this.update()
    );
  }

  resetFunc() {
    let def = { min: 0, max: 100 };
    let proteinDef = { min: 0, max: 20 };
    this.sortFunc("none");
    this.calFunc({ min: 0, max: 500 });
    this.fatFunc(proteinDef);
    this.carbFunc(def);
    this.proteinFunc(proteinDef);
    this.searchFunc("");
  }

  async nbdUpdate(index) {
    console.log("query" + this.state.query);
    let url =
      "https://www.unbeatable-food.com:5000/api/food/search/?pg=" +
      index +
      "&proteinmin=" +
      this.state.proteinValue.min +
      "&fatmin=" +
      this.state.fatValue.min +
      "&fatmax=" +
      this.state.fatValue.max +
      "&carbohydratemin=" +
      this.state.carbValue.min +
      "&carbohydratemax=" +
      this.state.carbValue.max +
      "&caloriemin=" +
      this.state.calValue.min +
      "&caloriemax=" +
      this.state.calValue.max +
      // ?number=<int>
      "&sortby=" + //(foodid, name, carbs, protein, fat, calories)
      this.state.sortBy +
      "&ascending=" +
      this.state.ascending +
      "&query=" +
      this.state.query;

    await fetch(url)
      .then(res => res.json())
      .then(data => {
        let foodItems = [];
        //for each differnet food item
        for (let item in data) {
          //for the nutrients in each food item
          for (let index in data[item]) {
            let totalNutrients = [];

            //for each individual nutrient in the food item
            for (let nutrient in data[item][index]["nutrients"]) {
              let nutrientItem = data[item][index]["nutrients"][nutrient];
              let singleNutrient = [];
              singleNutrient.push({
                name: nutrientItem.name,
                amount: nutrientItem.amount,
                unit: nutrientItem.unit
              });
              //add to this food items list
              totalNutrients.push({
                singleNutrient: singleNutrient
              });
            }

            //all information about
            foodItems.push({
              name: data[item][index]["food"].name,
              amount: data[item][index]["food"].amount,
              unit: data[item][index]["food"].unit,
              foodid: data[item][index]["food"].foodid,
              totalNutrients: totalNutrients
            });
          }
        }

        this.setState(
          {
            foodItems: foodItems
          },
          () => this.updateComps()
        );
      })
      .catch(error => {
        console.log(error);
        let recipes = [];
        this.setState(
          {
            recipes: recipes
          },
          () => this.updateComps()
        );
      });
  }

  pageUpdate(newPage) {
    this.setState({
      currPage: newPage
    });

    console.log("updating to" + newPage);

    this.nbdUpdate(newPage);
  }

  updateComps() {
    console.log("parent query" + this.state.query);
    //this is passing the wrong set of foodItems in
    let comps = this.state.foodItems.map(item => (
      //   //can this just pass in the item and then be parsed in the component?
      <NutritionLandingComponent
        id={item.foodid}
        item={item}
        highlighted={this.state.query}
      />
    ));

    this.setState({
      comps: comps
    });
  }

  render() {
    return (
      <div id="nutrition_landing" className="landingPage">
        <div>
          <img class="d-block w-100" src={green} alt="..." />
          <div>
            <br />
            <br />
            <h1 className="display-3">Nutrition Facts</h1>
            <p className="blockquote">
              Maximize your health by finding out exactly what you are putting
              in your body.
            </p>

            <NutritionFilterBar
              sortFunc={this.sortFunc}
              calFunc={this.calFunc}
              fatFunc={this.fatFunc}
              carbFunc={this.carbFunc}
              proteinFunc={this.proteinFunc}
              searchFunc={this.searchFunc}
              resetFunc={this.resetFunc}
              calValue={this.state.calValue}
              fatValue={this.state.fatValue}
              carbValue={this.state.carbValue}
              proteinValue={this.state.proteinValue}
            />

            <div className="landingPageGrid" id="title_text">
              <div className="card-columns">{this.state.comps}</div>
            </div>
          </div>
        </div>
        <PaginationElement
          currPage={this.state.currPage}
          numPages={5}
          changePage={this.pageUpdate}
        />
      </div>
    );
  }
}

export default NutritionLanding;
