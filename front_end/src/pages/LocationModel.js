import React, { Component } from "react";
import { Link } from "react-router-dom";
import pizza from "../assets/PizzaLoading.gif";
import star from "../assets/star.png";

class LocationModel extends Component {
  constructor(props) {
    super(props);

    this.state = {
      locationId: this.props.match.params.locationId,
      image: pizza
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.setState(nextProps);
    }
    this.database();
  }

  componentDidMount() {
    this.database();
  }

  database() {
    let url =
      "https://www.unbeatable-food.com:5000/api/location/" +
      this.state.locationId;
    var items = [];
    var recipeItems = [];
    const location_request = async () => {
      const response = await fetch(url);
      const json = await response.json();
      for (var i in json["items"]) {
        let current_food = json["items"][i];
        const food_request = await fetch(
          "https://www.unbeatable-food.com:5000/api/food/" +
            current_food["foodid"]
        );
        const food_data = await food_request.json();
        items.push([
          food_data["food"]["name"],
          "/nutrition_facts/" + current_food["foodid"],
          json
        ]);
      }
      for (var i in json["recipes"]) {
        let current_recipe = json["recipes"][i];
        const recipe_request = await fetch(
          "https://www.unbeatable-food.com:5000/api/recipe/" +
            current_recipe["recipeid"]
        );
        const recipe_data = await recipe_request.json();
        recipeItems.push([
          recipe_data["recipe"]["name"],
          "/recipes/" + current_recipe["recipeid"],
          json
        ]);
      }

      console.log(json);
      this.setState({
        name: json["location"]["name"],
        image: json["location"]["image"],
        phone: json["location"]["number"],
        address: json["location"]["address"],
        description: json["location"]["description"],
        price: json["location"]["price"],
        rating: json["location"]["rating"],
        storetype: json["location"]["storetype"],
        items: items,
        recipeItems: recipeItems
      });
    };
    location_request();
  }

  render() {
    let stars = [];
    for (let i = 0; i < this.state.rating; i++) {
      stars.push(<img src={star} style={{ width: "30", height: "30px" }} />);
    }

    let price = [];
    for (let i = 0; i < this.state.price; i++) {
      price += "$";
    }

    return (
      <div>
        <div>
          <br />
          <div className="bg-secondary line" />
          <br />
          <h1>{this.state.name}</h1>
          <br />
          <div className="bg-secondary line" />
          <br />
          <div />
        </div>{" "}
        <div>
          <img height="300vw" src={this.state.image} alt="location" />
        </div>
        <p>{this.state.address}</p>
        <p>{this.state.description}</p>
        <p>{this.state.phone}</p>
        <p>{stars}</p>
        <p>{this.state.storetype}</p>
        <p>{price}</p>
        <div>
          <div class="available">
            <div class="items">
              <h5 class="border rounded-top p-2">Available Items</h5>
              <table class="table">
                {this.state.items &&
                  this.state.items.map(postDetail => {
                    return (
                      <tr>
                        <td>
                          <Link
                            class="text-dark"
                            style={{ width: "100%", height: "100%" }}
                            id="click"
                            to={postDetail[1]}
                          >
                            {postDetail[0]}
                          </Link>
                        </td>
                      </tr>
                    );
                  })}
              </table>
            </div>

            <div class="items">
              <h5 class="border rounded-top p-2">Recipes</h5>
              <table class="table">
                {this.state.recipeItems &&
                  this.state.recipeItems.map(postDetail => {
                    return (
                      <tr>
                        <td class="text-light">
                          <Link class="text-dark" id="click" to={postDetail[1]}>
                            {postDetail[0]}
                          </Link>
                        </td>
                      </tr>
                    );
                  })}
              </table>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default LocationModel;
