import React, { Component } from "react";
import bakery from "../assets/Bakery.jpg";
import "../index.css";
import PaginationElement from "../components/PaginationElement";
import LocationLandingComponent from "../components/LocationLandingComponent";
import LocationFilterBar from "../components/LocationFilterBar";

class LocationsLanding extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nbd_numbers: [],
      currPage: 1,
      list: [],
      userSearch: "",
      sortBy: "none",
      ascending: true,
      locations: [],
      price: "",
      rating: 0,
      storeType: ""
    };

    this.pageUpdate = this.pageUpdate.bind(this);
    this.sortFunc = this.sortFunc.bind(this);
    this.storeFunc = this.storeFunc.bind(this);
    this.priceFunc = this.priceFunc.bind(this);
    this.ratingFunc = this.ratingFunc.bind(this);
    this.searchFunc = this.searchFunc.bind(this);
  }

  async componentDidMount() {
    await this.nbdUpdate(this.state.currPage);
    this.updateComps();
  }

  nbdUpdate(index) {
    let url =
      "https://www.unbeatable-food.com:5000/api/location/search/?pg=" +
      index +
      "&sortby=" +
      this.state.sortBy +
      "&ascending=" +
      this.state.ascending +
      "&storetype=" +
      this.state.storeType +
      "&price=" +
      this.state.price +
      "&query=" +
      this.state.userSearch;
    // "&rating=" +
    // this.state.rating;

    fetch(url)
      .then(res => res.json())
      .then(data => {
        let locations = [];
        // let newState = [];
        for (let item in data) {
          for (let index in data[item]) {
            let thisLocation = data[item][index];
            //for items
            let locationFoods = [];
            for (let food in thisLocation.items) {
              locationFoods.push(thisLocation.items[food].foodid);
            }

            locations.push({
              name: thisLocation.location.name,
              address: thisLocation.location.address,
              description: thisLocation.location.description,
              locationid: thisLocation.location.locationid,
              image: thisLocation.location.image,
              number: thisLocation.location.number,
              price: thisLocation.location.price,
              rating: thisLocation.location.rating,
              storetype: thisLocation.location.storetype,
              locationFoods: locationFoods
            });
          }
        }

        this.setState(
          {
            locations: locations
          },
          () => this.updateComps()
        );
      })
      .catch(error => {
        console.log(error);
        let locations = [];
        this.setState(
          {
            locations: locations
          },
          () => this.updateComps()
        );
      });
  }

  sortFunc(sortBy) {
    let sortId = "";
    let ascending = true;
    //let ids = ["locationid", "name", "rating", "price"];
    switch (sortBy) {
      case "Name A-Z":
        sortId = "name";
        break;

      case "Name Z-A":
        sortId = "name";
        ascending = false;
        break;

      case "Price Low-High":
        sortId = "price";
        break;

      case "Price High-Low":
        sortId = "price";
        ascending = false;
        break;

      case "Rating Low-High":
        sortId = "rating";
        break;

      case "Rating High-Low":
        sortId = "rating";
        ascending = false;
        break;
    }
    this.setState(
      {
        sortBy: sortId,
        asccending: ascending
      },
      () => this.update()
    );
  }

  priceFunc(dollars) {
    let price = dollars.length;
    this.setState(
      {
        price: price
      },
      () => this.update()
    );
  }

  ratingFunc(rating) {
    this.setState(
      {
        rating: rating
      },
      () => this.update()
    );
  }

  storeFunc(type) {
    this.setState(
      {
        storeType: type
      },
      () => this.update()
    );
  }

  searchFunc(userSearch) {
    this.setState(
      {
        userSearch: userSearch
      },
      () => this.update()
    );
  }

  resetFunc() {
    this.sortFunc("none");
    this.priceFunc("$$$$");
    this.storeFunc("");
    this.ratingFunc(0);
    this.searchFunc("");
  }

  pageUpdate(newPage) {
    this.setState({
      currPage: newPage
    });

    this.nbdUpdate(newPage);
  }

  async update() {
    await this.nbdUpdate(this.state.currPage);
    this.updateComps();
  }

  updateComps() {
    //this is passing the wrong set of foodItems in
    let comps = this.state.locations.map(item => (
      <LocationLandingComponent
        id={item.locationid}
        item={item}
        highlighted={this.state.userSearch}
      />
    ));

    this.setState({
      comps: comps
    });
  }

  render() {
    return (
      <div id="location_landing" className="landingPage">
        <div>
          <img class="d-block w-100" src={bakery} alt="..." />
          <div>
            <br />
            <br />
            <h1 className="display-3">Locations</h1>
            <p className="blockquote">Find where to get food near you.</p>
            <LocationFilterBar
              resetFunc={this.resetFunc}
              sortFunc={this.sortFunc}
              priceFunc={this.priceFunc}
              ratingFunc={this.ratingFunc}
              storeFunc={this.storeFunc}
              searchFunc={this.searchFunc}
            />

            <div className="landingPageGrid" id="title_text">
              <div className="card-columns">{this.state.comps}</div>
            </div>
          </div>
        </div>
        <PaginationElement
          currPage={this.state.currPage}
          numPages={4}
          changePage={this.pageUpdate}
        />
      </div>
    );
  }
}

export default LocationsLanding;
