import React, { Component } from "react";
import spoon from "../assets/SaltSpoon.jpg";
import "../index.css";
import PaginationElement from "../components/PaginationElement";
import RecipeLandingComponent from "../components/RecipeLandingComponent";
import RecipeFilterBar from "../components/RecipeFilterBar";

class RecipeLanding extends Component {
  constructor(props) {
    super(props);

    let compsImg = spoon;

    this.state = {
      nbd_numbers: [],
      currPage: 1,
      list: [],
      categories: [],
      currCategory: "",
      timeValue: { min: 0, max: 100 },
      sortBy: "none",
      ascending: "True",
      diet_list: ["vegan", "keto"],
      dietsChecked: [0, 0],
      recipes: [],
      comps: compsImg,
      userSearch: ""

      // searchString: "i",
      // dietString: "tmpe",
      // userInput: "e"
    };

    this.pageUpdate = this.pageUpdate.bind(this);
    this.timeFunc = this.timeFunc.bind(this);
    this.sortFunc = this.sortFunc.bind(this);
    this.searchFunc = this.searchFunc.bind(this);
    this.resetFunc = this.resetFunc.bind(this);
    this.getCheckedDiets = this.getCheckedDiets.bind(this);
    this.categoryFunc = this.categoryFunc.bind(this);

    // this.getSearchFieldInput = this.getSearchFieldInput.bind(this);
  }

  async componentDidMount() {
    let url = "https://www.unbeatable-food.com:5000/api/recipe/categories/";

    fetch(url)
      .then(res => res.json())
      .then(data => {
        let newState = [];
        for (let item in data) {
          for (let index in data[item]) {
            newState.push(data[item][index].category);
          }
        }
        this.setState({
          categories: newState
        });
      });

    await this.nbdUpdate(this.state.currPage);
    this.updateComps();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.setState(nextProps);
    }
  }

  nbdUpdate(index) {
    let url =
      "https://www.unbeatable-food.com:5000/api/recipe/search/?pg=" +
      index +
      "&maxtime=" +
      this.state.timeValue.max +
      "&mintime=" +
      this.state.timeValue.min +
      "&sortby=" + //(recipeid, name, servings, time)
      this.state.sortBy +
      "&ascending=" +
      this.state.ascending +
      "&query=" +
      this.state.userSearch +
      "&category=" +
      this.state.currCategory +
      "&vegan=" +
      this.isVegan() +
      "&keto=" +
      this.isKeto();

    fetch(url)
      .then(res => res.json())
      .then(data => {
        let recipes = [];
        //for each recipe
        for (let item in data) {
          for (let index in data[item]) {
            let thisRecipe = data[item][index];
            //loop through the categories
            let recipeCategories = [];
            for (let category in thisRecipe.category) {
              recipeCategories.push(thisRecipe.category[category].category);
            }
            //loop through the steps!
            let recipeSteps = [];
            for (let step in thisRecipe.steps) {
              recipeSteps.push({
                description: thisRecipe.steps[step].description,
                step: thisRecipe.steps[step].step
              });
            }

            //loop through the ingredients!
            let recipeIngredients = [];
            for (let ingredient in thisRecipe.ingredients) {
              recipeIngredients.push({
                amount: thisRecipe.ingredients[ingredient].amount,
                unit: thisRecipe.ingredients[ingredient].unit,
                foodid: thisRecipe.ingredients[ingredient].foodid
              });
            }

            if (thisRecipe != undefined) {
              recipes.push({
                name: thisRecipe.recipe.name,
                author: thisRecipe.recipe.author,
                authorabout: thisRecipe.recipe.authorabout,
                description: thisRecipe.recipe.description,
                imageurl: thisRecipe.recipe.imageurl,
                keto: thisRecipe.recipe.keto,
                recipeid: thisRecipe.recipe.recipeid,
                recipeyield: thisRecipe.recipe.recipeyield,
                sustainable: thisRecipe.recipe.sustainable,
                totaltime: thisRecipe.recipe.totaltime,
                vegan: thisRecipe.recipe.vegan,
                steps: recipeSteps,
                categories: recipeCategories,
                ingredients: recipeIngredients
              });
            }
          }
        }

        this.setState(
          {
            recipes: recipes
          },
          () => this.updateComps()
        );
      })
      .catch(error => {
        console.log(error);
        let recipes = [];
        this.setState(
          {
            recipes: recipes
          },
          () => this.updateComps()
        );
      });
  }

  pageUpdate(newPage) {
    this.setState({
      currPage: newPage
    });
    this.nbdUpdate(newPage);
  }

  isVegan() {
    if (this.state.dietsChecked[0] === 1) {
      return "True";
    }
    return "";
  }

  isKeto() {
    if (this.state.dietsChecked[1] === 1) {
      return "True";
    }
    return "";
  }

  timeFunc(timeValue) {
    this.setState(
      {
        timeValue: timeValue
      },
      () => this.update()
    );
  }

  sortFunc(sortBy) {
    let sortId = "";
    let ascending = "True";
    switch (sortBy) {
      case "Name A-Z":
        sortId = "name";
        break;

      case "Name Z-A":
        sortId = "name";
        ascending = "False";
        break;

      case "Cook Time Low-High":
        sortId = "time";
        break;

      case "Cook Time High-Low":
        sortId = "time";
        ascending = "False";
        break;

      case "Servings Low-High":
        sortId = "servings";
        break;

      case "Servings High-Low":
        sortId = "servings";
        ascending = "False";
        break;
    }
    this.setState(
      {
        sortBy: sortId,
        ascending: ascending
      },
      () => this.update()
    );
  }

  resetFunc() {
    this.timeFunc({ min: 0, max: 100 });
    this.sortFunc("none");
    this.categoryFunc("");
    this.getCheckedDiets([0, 0]);
    this.searchFunc("");
  }

  categoryFunc(category) {
    this.setState(
      {
        currCategory: category
      },
      () => this.update()
    );
  }

  getCheckedDiets(checks) {
    this.setState(
      {
        dietsChecked: checks
      },
      () => this.update()
    );
  }

  setSearch() {
    let search = "search?";
    let diet = "diet=" + this.state.dietString;
    this.setState(
      {
        searchString: search + diet
      },
      () => this.update()
    );
  }

  getSearchFieldInput(searchString) {
    this.setState(
      {
        userSearch: searchString
      },
      () => this.update()
    );
  }

  searchFunc(userSearch) {
    this.setState(
      {
        userSearch: userSearch
      },
      () => this.update()
    );
  }

  async update() {
    await this.nbdUpdate(this.state.currPage);
    this.updateComps();
  }

  updateComps() {
    let comps = [];

    // if (this.state.recipes.length > 0) {
    //this is passing the wrong set of foodItems in
    comps = this.state.recipes.map(item => (
      //   //can this just pass in the item and then be parsed in the component?
      <RecipeLandingComponent
        highlighted={this.state.userSearch}
        id={item.foodid}
        item={item}
      />
    ));
    // }

    this.setState({
      comps: comps
    });
  }

  render() {
    return (
      <div id="recipe_landing" className="landingPage">
        <div>
          <img class="d-block w-100" src={spoon} alt="..." />
          <div>
            <br />
            <br />
            <h1 className="display-3">Recipes</h1>
            <p className="blockquote">
              Find the perfect meal for any occassion.
            </p>

            <RecipeFilterBar
              timeValue={this.state.timeValue}
              timeFunc={this.timeFunc}
              categories={this.state.categories}
              currCategory={this.state.currCategory}
              sortFunc={this.sortFunc}
              resetFunc={this.resetFunc}
              searchFunc={this.searchFunc}
              categoryFunc={this.categoryFunc}
              diets={this.state.diet_list}
              getCheckedDiets={this.getCheckedDiets}
            />
            <div className="landingPageGrid" id="title_text">
              <div className="card-columns">
                <div class="w-100">{this.state.comps}</div>
              </div>
            </div>
          </div>
        </div>
        <PaginationElement
          currPage={this.state.currPage}
          numPages={5}
          changePage={this.pageUpdate}
        />
      </div>
    );
  }
}

export default RecipeLanding;
