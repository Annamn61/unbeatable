import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../index.css";
import Attributes from "../components/Attributes.js";
import pizza from "../assets/PizzaLoading.gif";

class RecipeModel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      recipeId: this.props.match.params.recipeId,
      image: pizza
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.setState(nextProps);
    }
    this.database();
  }

  componentDidMount() {
    this.database();
  }

  database() {
    let url =
      "https://www.unbeatable-food.com:5000/api/recipe/" + this.state.recipeId;
    var ingredients = [];
    var locations = [];
    const recipe_request = async () => {
      const response = await fetch(url);
      const json = await response.json();
      console.log(json);
      for (var i in json["ingredients"]) {
        let current_food = json["ingredients"][i];
        const food_request = await fetch(
          "https://www.unbeatable-food.com:5000/api/food/" +
            current_food["foodid"]
        );
        const food_data = await food_request.json();
        ingredients.push([
          current_food["amount"] + " " + current_food["unit"],
          food_data["food"]["name"],
          "/nutrition_facts/" + current_food["foodid"],
          json
        ]);
      }

      for (var i in json["locations"]) {
        let current_location = json["locations"][i];
        const location_request = await fetch(
          "https://www.unbeatable-food.com:5000/api/location/" +
            current_location["locationid"]
        );
        const location_data = await location_request.json();
        console.log(location_data);
        locations.push([
          location_data["location"]["name"],
          "/locations/" + current_location["locationid"],
          json
        ]);
      }

      this.setState({
        name: json["recipe"]["name"],
        image: json["recipe"]["imageurl"],
        time: json["recipe"]["totaltime"],
        description: json["recipe"]["description"],
        author: json["recipe"]["author"],
        steps: json["steps"],
        ingredients: ingredients,
        locations: locations
      });
    };
    recipe_request();
  }

  render() {
    return (
      <div id="recipe_landing" className="recipePage bg-light">
        <div className="recipeContent">
          <div>
            <br />
            <div className="bg-secondary line" />
            <br />
            <h1>{this.state.name}</h1>
            <br />
            <div className="bg-secondary line" />
            <br />
            <div />
          </div>

          <p>{this.state.description}</p>
          <p>By: {this.state.author}</p>

          <div>
            <img src={this.state.image} alt="Recipe Image" />
          </div>

          <br />
          <div className="bg-secondary line" />
          <br />

          <div class="row justify-content-start">
            <div class="col bg-dark text-light">
              <br />
              <h5 class="border rounded-top p-2">Ingredients</h5>
              <table class="table">
                {this.state.ingredients &&
                  this.state.ingredients.map(postDetail => {
                    return (
                      <tr>
                        <td class="text-left text-light">
                          <Link id="click" to={postDetail[2]}>
                            {postDetail[0] + " " + postDetail[1]}
                          </Link>
                        </td>
                      </tr>
                    );
                  })}
              </table>
            </div>

            <div class="col-6 border-top pl-2">
              <div class="pt-3">
                {this.state.steps &&
                  this.state.steps.map(postDetail => {
                    return (
                      <p>
                        <h4 class="text-left pl-2 border-bottom">
                          <p>Step {postDetail["step"]}</p>
                        </h4>
                        <p class="text-left"> {postDetail["description"]} </p>
                      </p>
                    );
                  })}
              </div>
            </div>

            <div class="col bg-dark text-light">
              <br />
              <h5 class="border rounded-top p-2">Locations</h5>
              <table class="table">
                {this.state.locations &&
                  this.state.locations.map(postDetail => {
                    return (
                      <tr>
                        <td class="text-light">
                          <Link id="click" to={postDetail[1]}>
                            {postDetail[0]}
                          </Link>
                        </td>
                      </tr>
                    );
                  })}
              </table>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default RecipeModel;
