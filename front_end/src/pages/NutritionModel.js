import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./model.css";

class NutritionModel extends Component {
  constructor(props) {
    super(props);

    this.state = {
      nutritionId: this.props.match.params.nutritionId,
      comps: [],
      vitamins: [],
      nutrients: []
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.setState(nextProps);
    }
    this.database();
  }

  async componentDidMount() {
    await this.database();
  }

  async database() {
    let url =
      "https://www.unbeatable-food.com:5000/api/food/" + this.state.nutritionId;
    var food = [];
    const recipe_request = async () => {
      const response = await fetch(url);
      const json = await response.json();

      const recipe_request = await fetch(
        "https://www.unbeatable-food.com:5000/api/recipe_with_food/" +
          this.state.nutritionId
      );

      const recipe_json = await recipe_request.json();

      var nutrients = [];
      var vitamins = [];
      var comps = [];
      for (let i in json["nutrients"]) {
        var name = json["nutrients"][i]["name"];
        console.log(name);
        if (name.includes("Vitamin")) {
          console.log("INCLUDES");
          vitamins.push({
            name: name,
            amount: json["nutrients"][i]["amount"],
            unit: json["nutrients"][i]["unit"]
          });
        } else {
          console.log("-----");

          nutrients.push({
            name: name,
            amount: json["nutrients"][i]["amount"],
            unit: json["nutrients"][i]["unit"]
          });
        }
      }

      console.log(vitamins);
      console.log(nutrients);

      let vits = vitamins.map(item => (
        <a href="https://www.precisionnutrition.com/all-about-vitamins-minerals">
          <div class="oneVit">
            <div class="circle">{item.name}</div>
            <div class="amounts">
              {item.amount} {item.unit}
            </div>
          </div>
        </a>
      ));

      comps = nutrients.map(item => (
        <p class="text-left border-bottom comps">
          Total {item.name}:{item.amount}
        </p>
      ));

      await this.setState({
        serving_size: json["food"]["amount"],
        unit: json["food"]["unit"],
        name: json["food"]["name"],
        nutrients: nutrients,
        linked_recipes: recipe_json,
        comps: comps,
        vitamins: vits
      });
    };
    recipe_request();
  }

  render() {
    console.log("rendering comps");

    return (
      <div>
        <div className="nutritionTitle bg-dark">
          <h4 class="display-3">{this.state.name}</h4>
        </div>
        {/* Have a card deck here that is two cards wide,
        one of the cards on the left is the image, then fill
        in the other cards with the facts about the data!
        Could have a graph that shows % daily values of
        different vitamins/minerals as well :) */}
        <div class="col">
          <div class="nutrition-top">
            <div class="nutrition-label border">
              <h2 class="border-bottom"> Nutrition Facts </h2>
              <div class="row border">
                <div class="col text-left">
                  <strong> Serving size </strong>
                </div>
                <div class="col text-right">
                  <strong>
                    {this.state.serving_size + " " + this.state.unit + "(s)"}
                  </strong>
                </div>
              </div>
              <p class="text-left mb-0">Amount per serving</p>
              <div class="d-flex justify-content-between border-bottom mb-2">
                <h4>Calories</h4>
                {/* <h4>
                  {this.state.nutrients &&
                    this.state.nutrients[0] &&
                    this.state.nutrients[0].amount}
                </h4> */}
              </div>
              <div>{this.state.comps}</div>
            </div>
            <div class="vitamins border">
              <h2>Vitamins</h2>
              <div class="allVits">{this.state.vitamins}</div>
            </div>
          </div>
          <div class="col align-right">
            <h2 class="text-left pl-5 pb-2 pt-2 border-bottom bg-green">
              Find this food in{" "}
            </h2>
            {this.state.linked_recipes &&
              this.state.linked_recipes.recipe.map(postDetail => {
                return (
                  <Link
                    class="text-dark"
                    id="click"
                    to={"/recipes/" + postDetail["recipeid"]}
                  >
                    <p class="foodin border-bottom">{postDetail["name"]}</p>
                  </Link>
                );
              })}
            <br />
          </div>
        </div>
      </div>
    );
  }
}

export default NutritionModel;
