import React, { Component } from "react";
import { Link } from "react-router-dom";

class Model3Component extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: props.name,
      image: props.image,
      address: props.address,
      price: props.price,
      type: props.type,
      m1Link: props.m1Link,
      m1Title: props.m1Title,
      m2Link: props.m2Link,
      m2Title: props.m2Title
    };
  }

  render() {
    return (
      <div>
        <p> {this.state.name} </p>
        <img src={this.state.image} />
        <p> {this.state.address} </p>
        <p> {this.state.price} </p>
        <p> {this.state.type} </p>
        <p><Link to={this.state.m1Link}>{this.state.m1Title}</Link></p>
        <p><Link to={this.state.m2Link}>{this.state.m2Title}</Link></p>
      </div>
    );
  }
}

export default Model3Component;