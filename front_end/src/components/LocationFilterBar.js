import React, { Component } from "react";
import "../index.css";

class LocationFilterBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sortItems: [
        "Name A-Z",
        "Name Z-A",
        "Price Low-High",
        "Price High-Low",
        "Rating Low-High",
        "Rating High-Low"
      ],
      priceOptions: ["$", "$$", "$$$"],
      storeOptions: ["Supermarket", "Grocery", "Convenience"],
      ratingOptions: [1, 2, 3, 4, 5],
      resetFunc: props.resetFunc,
      sortFunc: props.sortFunc,
      searchFunc: props.searchFunc,
      priceFunc: props.priceFunc,
      storeFunc: props.storeFunc,
      ratingFunc: props.ratingFunc
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.setState(nextProps);
    }
  }

  updateSearch() {
    let input = document.getElementById("search_location").value;
    this.state.searchFunc(input);
  }

  resetFiltering() {
    return true;
  }

  sortBy(item) {
    this.state.sortFunc(item);
  }

  filterPrice(item) {
    this.state.priceFunc(item);
  }

  filterRating(item) {
    this.state.ratingFunc(item);
  }

  filterStores(item) {
    this.state.storeFunc(item);
  }

  resetFiltering() {
    this.state.resetFunc();
  }

  render() {
    const sortByTabs = this.state.sortItems.map(item => (
      <a className="dropdown-item diet-item" onClick={() => this.sortBy(item)}>
        {item}
      </a>
    ));

    const noneTab = (
      <a
        className="dropdown-item diet-item"
        onClick={() => this.sortBy("none")}
      >
        none
      </a>
    );

    const priceTabs = this.state.priceOptions.map(item => (
      <a
        className="dropdown-item diet-item"
        onClick={() => this.filterPrice(item)}
      >
        {item}
      </a>
    ));

    const storeTabs = this.state.storeOptions.map(item => (
      <a
        className="dropdown-item diet-item"
        onClick={() => this.filterStores(item)}
      >
        {item}
      </a>
    ));

    const ratingTabs = this.state.ratingOptions.map(item => (
      <a
        className="dropdown-item diet-item"
        onClick={() => this.filterRating(item)}
      >
        {item}
      </a>
    ));

    return (
      <div className="selectorBar">
        <div className="dropdown show">
          <a
            className="btn dropdown-toggle"
            href="#"
            role="button"
            id="dropdownMenuLink"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            Sort by
          </a>

          <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
            {sortByTabs}
            {noneTab}
          </div>
        </div>

        <div className="dropdown show">
          <a
            className="btn dropdown-toggle"
            href="#"
            role="button"
            id="dropdownMenuLink"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            Filter Prices
          </a>

          <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
            {priceTabs}
          </div>
        </div>

        <div className="dropdown show">
          <a
            className="btn dropdown-toggle"
            href="#"
            role="button"
            id="dropdownMenuLink"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            Filter Store Types
          </a>

          <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
            {storeTabs}
          </div>
        </div>

        <div className="dropdown show">
          <a
            className="btn dropdown-toggle"
            href="#"
            role="button"
            id="dropdownMenuLink"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            Filter Rating
          </a>

          <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
            {ratingTabs}
          </div>
        </div>

        <div class="newStyle">
          <button
            type="button"
            className="btn my-2 my-sm-0"
            onClick={() => this.resetFiltering()}
          >
            Reset
          </button>
        </div>

        <div class="newStyle">
          <form class="form-inline">
            <input
              id="search_location"
              className="form-control mr-sm-2"
              type="search"
              placeholder="Search"
              aria-label="Search"
            />
            <button
              type="button"
              id="search_button"
              className="btn my-2 my-sm-0"
              onClick={() => this.updateSearch()}
            >
              Search
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default LocationFilterBar;
