import React, { Component } from "react";
import "../index.css";

class Highlighter extends Component {
  constructor(props) {
    super(props);

    this.state = {
      highlight: props.highlight.toUpperCase(),
      content: props.content.toUpperCase()
    };
  }

  componentWillMount() {
    this.setState({
      highlight: this.state.highlight.toUpperCase(),
      content: this.state.content.toUpperCase()
    });
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.setState(nextProps);
    }
  }

  getSplit(words) {
    let splitTerm = words;
    if (words != undefined) {
      splitTerm = words.toUpperCase();
    }
    return splitTerm;
  }

  render() {
    let splitTerm = this.getSplit(this.state.highlight);
    let sections = this.state.content.split(splitTerm);
    let display = sections.map((section, index) => {
      if (index < sections.length - 1) {
        return (
          <text>
            {section}
            <text class="highlight">{splitTerm}</text>
          </text>
        );
      } else {
        return <text>{section}</text>;
      }
    });

    return (
      <div>
        <div>{display}</div>
      </div>
    );
  }
}

export default Highlighter;
