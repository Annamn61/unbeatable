import React, { Component } from "react";
import "../index.css";
import InputRange from "react-input-range";
import "react-input-range/lib/css/index.css";

class RecipeFilterBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sortItems: [
        "Name A-Z",
        "Name Z-A",
        "Cook Time Low-High",
        "Cook Time High-Low",
        "Servings Low-High",
        "Servings High-Low"
      ],
      timeValue: props.timeValue,
      timeFunc: props.timeFunc,
      categories: props.categories,
      currCategory: props.currCategory,
      categoryFunc: props.categoryFunc,
      diets: props.diets,
      getCheckedDiets: props.getCheckedDiets,
      noCategory: "Category",
      sortFunc: props.sortFunc,
      searchFunc: props.searchFunc,
      resetFunc: props.resetFunc
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.setState(nextProps);
    }
  }

  updateSearch() {
    let input = document.getElementById("search_recipe").value;
    this.state.searchFunc(input);
  }

  sortBy(item) {
    this.state.sortFunc(item);
  }

  filterTime(item) {
    this.state.timeFunc(item);
  }

  resetFiltering() {
    this.state.resetFunc();
  }

  filterCategory(category) {
    this.setState({
      currCategory: category,
      noCategory: category
    });
    this.state.categoryFunc(category);
  }

  getFilters() {
    let status = document.getElementsByClassName("custom-control-input");

    let check = new Array(status.length);
    for (let i = 0; i < this.state.diets.length; i++) {
      if (status[i].checked) {
        check[i] = 1;
      } else {
        check[i] = 0;
      }
    }
    this.state.getCheckedDiets(check);
  }

  render() {
    const sortByTabs = this.state.sortItems.map(item => (
      <a className="dropdown-item diet-item" onClick={() => this.sortBy(item)}>
        {item}
      </a>
    ));

    const categoryTabs = this.state.categories.map(category => (
      <a
        className="dropdown-item diet-item noStyle"
        onClick={() => this.filterCategory(category)}
      >
        {category}
      </a>
    ));

    const noneTab = (
      <a
        className="dropdown-item diet-item"
        onClick={() => this.sortBy("none")}
      >
        none
      </a>
    );

    const checkboxes = this.state.diets.map((diet, index) => (
      <div className="custom-control custom-checkbox">
        <input
          type="checkbox"
          className="custom-control-input"
          id={"customCheck" + index}
        />
        <label className="custom-control-label" htmlFor={"customCheck" + index}>
          {diet}
        </label>
      </div>
    ));

    return (
      <div className="selectorBar">
        <div className="dropdown show">
          <a
            className="btn dropdown-toggle"
            href="#"
            role="button"
            id="dropdownMenuLink"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            Sort by
          </a>

          <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
            {sortByTabs}
            {noneTab}
          </div>
        </div>

        <div class="slideDiv">
          <div>Cook Time</div>

          <div className="slider">
            <InputRange
              maxValue={100}
              minValue={0}
              value={this.state.timeValue}
              onChange={timeValue => this.state.timeFunc(timeValue)}
            />
          </div>
        </div>

        <div className="dropdown show">
          <a
            className="btn dropdown-toggle"
            href="#"
            role="button"
            id="dropdownMenuLink"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            {this.state.noCategory}
          </a>

          <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
            {categoryTabs}
          </div>
        </div>

        <div class="newStyle">
          <button
            type="button"
            className="btn"
            data-toggle="modal"
            data-target="#exampleModal"
          >
            Select Diets
          </button>
        </div>

        <div class="newStyle">
          <button
            type="button"
            className="btn my-2 my-sm-0"
            onClick={() => this.resetFiltering()}
          >
            Reset
          </button>
        </div>

        <div class="flex1">
          <form class="form-inline">
            <input
              id="search_recipe"
              className="form-control mr-sm-2"
              type="search"
              placeholder="Search"
              aria-label="Search"
            />
            <button
              type="button"
              id="search_button"
              className="btn my-2 my-sm-0"
              onClick={() => this.updateSearch()}
            >
              Search
            </button>
          </form>
        </div>

        <div
          className="modal fade"
          id="exampleModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Select Filters
                </h5>
                <div className="checks modal-body">{checkboxes}</div>

                <div className="modal-footer newStyle">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    data-dismiss="modal"
                    onClick={() => this.getFilters()}
                  >
                    Apply
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default RecipeFilterBar;
