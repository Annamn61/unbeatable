import React, { Component } from "react";
import LandingComponent from "../pages/LandingComponent";

class NutritionPagination extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listData: ("hello", "hi", "hola"),
      nutritionItems: [],
      currPage: props.currPage,
      perPage: 10,
      apiData: []
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.setState(nextProps);
    }
  }

  componentDidMount() {
    fetch("https://jsonplaceholder.typicode.com/posts")
      .then(response => response.json())
      .then(data => {
        let newState = [];
        for (let item in data) {
          if (this.validIndex(item)) {
            newState.push({
              word: data[item].id
            });
          }
        }
        this.setState({
          apiData: newState
        });
      });

    let newState = [];
    for (let wordIndex in this.state.listData) {
      let wordLen = wordIndex;

      newState.push({
        word: this.state.listData[wordIndex],
        len: wordLen
      });
    }
    this.setState({
      nutritionItems: newState
    });
  }

  validIndex(item) {
    let start = (this.state.currPage - 1) * this.state.perPage;
    if (item >= start && item < start + this.state.perPage) {
      return true;
    }
    return false;
  }

  render() {
    const list = this.state.apiData.map(item => (
      <LandingComponent name={item.word} link="nutrition_facts/item" />
    ));

    return <div>{list}</div>;
  }
}

export default NutritionPagination;
