import React, { Component } from "react";
import "./About.css";

class Links extends Component {
  render() {
    return (
      <div className="linkContainer">
        <div className="linkBlocks">
          <div className="linkSquare">
            <a href="https://gitlab.com/Annamn61/unbeatable">GitLab</a>
          </div>
          <div className="linkSquare">
            <a href="https://documenter.getpostman.com/view/6748380/S11NMwfL">
              Postman
            </a>
          </div>
        </div>
        <div className="linkTitle">
          <h4 class="display-4">Links</h4>
        </div>
      </div>
    );
  }
}

export default Links;
