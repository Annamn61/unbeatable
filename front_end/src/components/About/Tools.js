import React, { Component } from "react";
import "./About.css";

class Tools extends Component {
  render() {
    return (
      <div className="toolContainer">
        <div className="toolTitle">
          <h4 class="display-4">Tools</h4>
        </div>
        <div className="toolBlocks">
          <div className="toolLine">
            <div className="toolSquare">
              <a href="https://reactjs.org/">
                <p> React </p>
              </a>
            </div>
            <div className="toolSquare">
              <a href="https://facebook.github.io/create-react-app/">
                <p> create-react-app </p>
              </a>
            </div>
            <div className="toolSquare">
              <a href="https://getbootstrap.com/">
                <p> Bootstrap </p>
              </a>
            </div>
            <div className="toolSquare">
              <a href="https://developer.mozilla.org/en-US/docs/Glossary/CSS">
                <p> CSS </p>
              </a>
            </div>
            <div className="toolSquare">
              <a href="https://www.nginx.com/">
                <p> nginx </p>
              </a>
            </div>
            <div className="toolSquare">
              <a href="https://www.npmjs.com/package/react-input-range">
                <p> react-input-range </p>
              </a>
            </div>
            <div className="toolSquare">
              <a href="https://reacttraining.com/react-router/">
                <p> react-router </p>
              </a>
            </div>
          </div>

          <div className="toolLine">
            <div className="toolSquare">
              <a href="https://www.npmjs.com/">
                <p> npm </p>
              </a>
            </div>
            <div className="toolSquare">
              <a href="http://chromedriver.chromium.org/">
                <p> chromedriver </p>
              </a>
            </div>
            <div className="toolSquare">
              <a href="https://www.pgadmin.org/">
                <p> pgAdmin </p>
              </a>
            </div>
            <div className="toolSquare">
              <a href="https://gitlab.com/">
                <p> gitlab </p>
              </a>
            </div>
            <div className="toolSquare">
              <a href="https://aws.amazon.com/s3/">
                <p> AWS s3 </p>
              </a>
            </div>
            <div className="toolSquare">
              <a href="https://www.docker.com/">
                <p> docker </p>
              </a>
            </div>
            <div className="toolSquare">
              <a href="https://www.getpostman.com/">
                <p> postman </p>
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Tools;
