import React, { Component } from "react";
import "./About.css";

class Data extends Component {
  render() {
    return (
      <div className="dataContainer">
        <div className="dataBlocks">
          <div className="dataSquare">
            <a href="https://www.edamam.com/">Recipes</a>
          </div>
          <div className="dataSquare">
            <a href="https://www.fatsecret.com/calories-nutrition/">
              Nutrition Facts
            </a>
          </div>
          <div className="dataSquare">
            <a href="https://spoonacular.com/api/docs/menu-items-api">
              Locations
            </a>
          </div>
          <div className="dataTitle">
            <h4 class="display-4">Data</h4>
          </div>
        </div>
      </div>
    );
  }
}

export default Data;
