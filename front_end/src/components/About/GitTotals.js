import React, { Component } from "react";
import "./About.css";
import git from "../../assets/GitlabBW.png";

class GitTotals extends Component {
  constructor() {
    super();

    this.state = {
      issues: 0,
      commits: 0,
      unitTests: 30
    };
  }

  setDataArray(data) {
    data.map(this.sumIssues.bind(this));
  }

  sumIssues(issue, index) {
    var issueSum = 0;

    if (issue.closed_by != null) {
      issueSum = 1;
    }

    issueSum = this.state.issues + issueSum;

    this.setState({ issues: issueSum });
  }

  setCommitArray(data) {
    data.map(this.sumCommits.bind(this));
  }

  sumCommits(commit, index) {
    var commitSum = this.state.commits + 1;

    this.setState({ commits: commitSum });
  }

  getCommits() {
    fetch(
      "https://gitlab.com/api/v4/projects/11080900/issues?private_token=NDgSgVZg1AU8Zz9qmn1E"
    )
      .then(res => res.json()) //response type
      .then(data => this.setDataArray(data));

    return this.state.commits;
  }

  componentDidMount() {
    fetch(
      "https://gitlab.com/api/v4/projects/11080900/issues?per_page=290&private_token=NDgSgVZg1AU8Zz9qmn1E"
    )
      .then(res => res.json()) //response type
      .then(data => this.setDataArray(data));

    fetch(
      "https://gitlab.com/api/v4/projects/11080900/repository/commits?all=true&per_page=290&private_token=NDgSgVZg1AU8Zz9qmn1E"
    )
      .then(res => res.json()) //response type
      .then(data => this.setCommitArray(data));
    // Note: it's important to handle errors here
    // instead of a catch() block so that we don't swallow
    // exceptions from actual bugs in components.
  }

  render() {
    return (
      <div className="dataContainer bg-dark">
        <div className="dataTitle">
          <img className="dataImg" src={git} alt="git" />
          <h4 class="display-4">Totals</h4>
        </div>
        <div className="dataBlocks">
          <div className="gitSquare">
            <h4 class="display-4">{this.state.issues}</h4>
            <p> Issues Closed</p>
          </div>
          <div className="gitSquare">
            <h4 class="display-4">{this.state.commits}</h4>
            <p> Commits</p>
          </div>
          <div className="gitSquare">
            <h4 class="display-4">{this.state.unitTests}</h4>
            <p> Unit Tests</p>
          </div>
        </div>
      </div>
    );
  }
}

export default GitTotals;
