import React, { Component } from "react";
import "./About.css";
import "bootstrap";

class MemberComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: props.name,
      image: props.image,
      bio: props.bio,
      resp: props.majorResp,
      gitId: props.gitId,
      issues: 0,
      commits: 0,
      ubuntu: 0,
      unitTests: props.unitTests
    };
  }

  setDataArray(data) {
    data.map(this.sumIssues.bind(this));
  }

  sumIssues(issue, index) {
    var issueSum = 0;

    if (
      issue.closed_by != null &&
      issue.closed_by.username === this.state.gitId
    ) {
      issueSum = 1;
    }

    issueSum = this.state.issues + issueSum;

    this.setState({ issues: issueSum });
  }

  setCommitArray(data) {
    data.map(this.sumCommits.bind(this));
  }

  sumCommits(commit, id, index) {
    var commitSum = 0;

    if (
      commit.committer_name === this.state.name ||
      commit.committer_name === this.state.gitId
    ) {
      commitSum = 1;
    }

    commitSum = this.state.commits + commitSum;

    this.setState({ commits: commitSum });
    return commitSum;
  }

  ubuntuCommitArray(data) {
    data.map(this.sumUbuntu.bind(this));
  }

  sumUbuntu(commit, index) {
    console.log("summing ubuntu");
    var ubuntuSum = 0;

    if (commit.committer_name === "Ubuntu") {
      ubuntuSum = 1;
    }

    ubuntuSum = this.state.ubuntu + ubuntuSum;

    this.setState({ ubuntu: ubuntuSum });
  }

  async componentDidMount() {
    fetch(
      "https://gitlab.com/api/v4/projects/11080900/issues?per_page=290&private_token=NDgSgVZg1AU8Zz9qmn1E"
    )
      .then(res => res.json()) //response type
      .then(data => this.setDataArray(data));

    fetch(
      "https://gitlab.com/api/v4/projects/11080900/repository/commits?all=true&per_page=290&private_token=NDgSgVZg1AU8Zz9qmn1E"
    )
      .then(res => res.json()) //response type
      .then(data => this.setCommitArray(data));
    // Note: it's important to handle errors here
    // instead of a catch() block so that we don't swallow
    // exceptions from actual bugs in components.

    await fetch(
      "https://gitlab.com/api/v4/projects/11080900/repository/commits?all=true&per_page=290&private_token=NDgSgVZg1AU8Zz9qmn1E"
    )
      .then(res => res.json()) //response type
      .then(data => this.ubuntuCommitArray(data));

    if (this.state.gitId == "Jacob_Hung" || this.state.gitId === "corykacal") {
      console.log(this.state.gitId + " " + this.state.ubuntu);
      let commits = this.state.commits;
      let ubuntu = this.state.ubuntu / 2;
      this.setState({
        commits: commits + parseInt(ubuntu)
      });
    }
  }

  render() {
    return (
      <div className="member col-2 bg-dark h-10 d-flex">
        <div class="card mb-4">
          <img
            className="imageStyle img-fluid"
            src={this.state.image}
            alt={this.state.gitId}
          />
          <div class="card-body">
            <h5 class="card-title">{this.state.name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">{this.state.resp}</h6>
            <p class="card-text">{this.state.bio}</p>
            <div class="container">
              <p> Issues Closed: {this.state.issues} </p>
              <p> Commits: {this.state.commits} </p>
              <p> Unit Tests: {this.state.unitTests} </p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default MemberComponent;
