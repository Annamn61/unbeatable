import React, { Component } from "react";
import MemberComponent from "./MemberComponent";
import GitTotals from "./GitTotals";
import Data from "./Data";
import Tools from "./Tools";
import Links from "./Links";

import anna_image from "../../assets/Anna.png";
import chris_image from "../../assets/Chris.png";
import cory_image from "../../assets/Cory.png";
import turner_image from "../../assets/Turner.jpeg";
import jacob_image from "../../assets/Jacob.png";
import yellow from "../../assets/YellowPrep.jpg";
import "./About.css";

class About extends Component {
  render() {
    return (
      <div id="about_landing" className="aboutPage bg-dark">
        <div>
          <img class="d-block w-100" src={yellow} alt="..." />
        </div>
        <div className="titleDiv bg-dark">
          <h1 className="display-3">Our Mission</h1>
          <br />
          <p className="blockquote">
            We at UnbEATable are about one simple thing: letting the people know
            exactly what they're putting in their bodies. Healthy eating can be
            a challenge when it's difficult to find nutrition facts for the food
            on your plate, and we think that affordability shouldn't be a factor
            that stops you from giving your body what it deserves. By putting
            emphasis on eating locally, and eating in season, UnbEATable will
            promote healthy eating that is not only affordable but great for the
            environment!
          </p>
          <br />
          <div className="yellow" />
          <br />
          <h1 className="display-3">Meet the Team</h1>
          <br />
        </div>
        <div class="container-fluid content-row bg-dark">
          <div className="members card-group row">
            <MemberComponent
              image={anna_image}
              name="Anna Norman"
              gitId="Annamn61"
              bio="Hi! I'm a second year computer science major. I have a passion for running, and I love to cook!"
              majorResp="Front-end"
              unitTests="10"
            />
            <MemberComponent
              name="Turner Gregory"
              image={turner_image}
              gitId="turnergregs"
              bio="Hi! I'm a third year computer science major. I am also super tall."
              majorResp="Front-end"
              unitTests="20"
            />
            <MemberComponent
              name="Chris Hamill"
              image={chris_image}
              gitId="wchamill"
              bio="Hi! I'm a third year computer science major. I am a former woodsman, and I like long walks on the beach."
              majorResp="Full-Stack"
              unitTests="36"
            />
            <MemberComponent
              name="Jacob Hungerford"
              image={jacob_image}
              gitId="Jacob_Hung"
              bio="Hi! I'm a third year computer science major. I enjoy electronic dance music and video games."
              majorResp="Back-end"
              unitTests="24"
            />
            <MemberComponent
              name="Cory Kacal"
              image={cory_image}
              gitId="corykacal"
              bio="Hi! I'm a fourth year computer science major. I like smoking meats."
              majorResp="Back-end"
              unitTests="12"
            />
          </div>
          <br />
        </div>
        <div className="bg-dark aboutGrid">
          <div className="yellow" />
          <br />
          <GitTotals />
          <br />
          <Data />
          <br />
          <Tools />
          <br />
          <Links />
          <br />
          <div className="yellow" />
          <br />
        </div>
      </div>
    );
  }
}

export default About;
