import React, { Component } from "react";
import { Link } from "react-router-dom";
import H from "../components/Highlighter";
import "../index.css";
import loader from "../assets/PizzaLoading.gif"

class RecipeLandingComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: props.id,
      name: "",
      highlighted: props.highlighted,
      item: props.item,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.setState(nextProps, () => this.database());
    }
  }

  componentDidMount() {
    this.database();
  }

  database() {
    let item = this.state.item;
    let ingredients = item.recipeIngredients;
    let steps = item.recipeSteps;
    let category = item.recipeCategories;

    this.setState({
      name: item.name,
      image: item.imageurl,
      id: item.recipeid,
      yield: item.recipeyield,
      time: item.totaltime,
      description: item.description,
      author: item.author,
    });
  }

  render() {
    let link = "/recipes/" + this.state.id;
    return (
      <div class="card bg-light text-black here">
        <Link to={link}>
          {" "}
          <img class="card-img-top" src={this.state.image} />{" "}
        </Link>
        <div class="card-body card-header">
          <h5 class="card-title">
            <H
              id="title_text"
              highlight={this.state.highlighted.toUpperCase()}
              content={this.state.name.toUpperCase()}
            />
          </h5>
          <p class="card-text">Serves {this.state.yield}</p>
          <p class="card-text">{this.state.time} min</p>
          <p class="card-text">by {this.state.author}</p>
        </div>
      </div>
    );
  }
}

export default RecipeLandingComponent;
