import React, { Component } from "react";
import { Link } from "react-router-dom";
import H from "../components/Highlighter";

class LocationLandingComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: props.id,
      name: "",
      description: "",
      item: props.item,
      highlighted: props.highlighted
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.setState(nextProps, () => this.database());
    }
  }

  componentDidMount() {
    this.database();
  }

  database() {
    let location = this.state.item;
    this.setState({
      name: location.name,
      image: location.image,
      address: location.address,
      description: location.description,
      storetype: location.storetype,
      price: location.price,
      rating: location.rating
    });
  }

  getDollars() {
    let s = "";
    for (let i = 0; i < this.state.price; i++) {
      s += "$";
    }
    return s;
  }

  render() {
    let link = "/locations/" + this.state.id;
    let dollars = this.getDollars();
    return (
      <div id="location_component" class="card mb-4 bg-light text-black">
        <Link to={link}>
          {" "}
          <img
            class="card-img-top location-image"
            src={this.state.image}
          />{" "}
        </Link>
        <div class="card-body card-header">
          <h5 class="card-title">
            <H
              id="title_text"
              highlight={this.state.highlighted}
              content={this.state.name.toUpperCase()}
            />
          </h5>
          <p class="card-text">
            <H
              id="title_text"
              highlight={this.state.highlighted}
              content={this.state.description.toUpperCase()}
            />
          </p>
          <p>{this.state.address}</p>
          <p>Rating: {this.state.rating}</p>
          <p>{dollars}</p>
        </div>
      </div>
    );
  }
}

export default LocationLandingComponent;
