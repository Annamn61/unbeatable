import React, { Component } from "react";
import { Link } from "react-router-dom";

class NavBar extends Component {
  constructor() {
    super();
    this.state = {
      search: "/search/a"
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.setState(nextProps);
    }
  }

  async search() {
    let input = document.getElementById("nav_search_id").value;
    await this.setState({
      search: "/search/" + input
    });
  }

  render() {
    return (
      <div>
        <nav className="navbar navbar-expand-sm navbar-light bg-light">
          <Link id="home_nav" className="navbar-brand" to="/home">
            unbEATable
          </Link>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li class="nav-item active">
                <Link id="recipe_nav" class="nav-link" to="/RecipeLand">
                  Recipes <span class="sr-only">(current)</span>
                </Link>
              </li>

              <li class="nav-item active">
                <Link id="nutrition_nav" class="nav-link" to="/NutritionLand">
                  Nutrition Facts <span class="sr-only">(current)</span>
                </Link>
              </li>

              <li class="nav-item active">
                <Link id="location_nav" class="nav-link" to="/LocationLand">
                  Locations <span class="sr-only">(current)</span>
                </Link>
              </li>
              <li class="nav-item active">
                <Link id="about_nav" class="nav-link" to="/About">
                  About <span class="sr-only">(current)</span>
                </Link>
              </li>
              <li class="nav-link active">
                <a href="/Visualization/">
                  <p class="linkColor">Servings</p>
                </a>
              </li>
              <li class="nav-link active">
                <a href="/Visualization/">
                  <p class="linkColor">Common Foods</p>
                </a>
              </li>
              <li class="nav-link active">
                <a href="/Visualization/">
                  <p class="linkColor">Vis 3</p>
                </a>
              </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="form-inline navbar-right">
                <input
                  type="text"
                  id="nav_search_id"
                  placeholder="Search..."
                  className="form-control mr-sm-2"
                  onChange={() => this.search()}
                />
                <Link id="search_nav" to={this.state.search}>
                  <button
                    id="everything_search"
                    type="button"
                    className="btn my-2 my-sm-0"
                  >
                    Search
                  </button>
                </Link>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}

export default NavBar;
