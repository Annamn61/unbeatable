import React, { Component } from "react";
import "../index.css";
import { withRouter } from "react-router-dom";

class PageItem extends Component {
  /* TODO: Set all items to have a tint on hover
        Shouldn't be able to click on current page */

  constructor(props) {
    super(props);
    this.state = {
      active: props.active,
      pageName: props.pageName
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.setState(nextProps);
    }
  }

  handleClick = () => {
    this.props.onClick();
  };

  render() {
    const activePage = (
      <li class="page-item">
        <div
          class="page-link text-light bg-dark border-dark"
          onClick={this.handleClick}
        >
          {this.state.pageName}
        </div>
      </li>
    );

    const inactivePage = (
      <li class="page-item">
        <div class="page-link text-dark border-dark" onClick={this.handleClick}>
          {this.state.pageName}
        </div>
      </li>
    );

    let retPage = this.state.active ? activePage : inactivePage;

    return retPage;
  }
}

class PaginationElement extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currPage: props.currPage,
      numPages: props.numPages,
      pagItems: []
    };

    this.paginationClick = this.paginationClick.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.setState(nextProps);
    }
    this.paginationLoad();
  }

  paginationLoad() {
    let pageinationPages = [];
    for (let page = 1; page <= this.state.numPages; page++) {
      pageinationPages.push({
        pageName: page,
        active: page === this.state.currPage,
        onClick: () => this.paginationClick(page)
      });
    }
    this.setState({
      pagItems: pageinationPages
    });
  }

  componentDidMount() {
    this.paginationLoad();
  }

  paginationClick(page) {
    this.setState(
      {
        currPage: page
      },
      this.props.changePage(page)
    );
  }

  render() {
    const pageTabs = this.state.pagItems.map(item => (
      <PageItem
        pageName={item.pageName}
        active={item.active}
        onClick={item.onClick}
      />
    ));

    let prev = this.state.currPage - 1;
    prev = prev > 0 ? prev : this.state.currPage;
    let next = this.state.currPage + 1;
    next = next <= this.state.numPages ? next : this.state.currPage;

    return (
      <div>
        <nav aria-label="Page navigation example">
          <ul class="pagination justify-content-center">
            <PageItem
              active={false}
              pageName="First"
              onClick={() => this.paginationClick(1)}
            />
            <PageItem
              active={false}
              pageName="Previous"
              onClick={() => this.paginationClick(prev)}
            />
            {pageTabs}
            <PageItem
              active={false}
              pageName="Next"
              onClick={() => this.paginationClick(next)}
            />
            <PageItem
              active={false}
              pageName="Last"
              onClick={() => this.paginationClick(this.state.numPages)}
            />
          </ul>
        </nav>
      </div>
    );
  }
}

export default withRouter(PaginationElement);
