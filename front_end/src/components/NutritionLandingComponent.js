import React, { Component } from "react";
import { Link } from "react-router-dom";
import H from "../components/Highlighter";

class NutritionLandingComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: props.item,
      foodid: props.item.foodid,
      name: "",
      highlighted: props.highlighted
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.setState(nextProps, () => this.database());
    }
  }

  componentDidMount() {
    this.database();
  }

  database() {
    let item = this.state.item;
    let nutr = item.totalNutrients;
    this.setState({
      name: item.name,
      calories: nutr[0].singleNutrient[0].amount,
      unit: item.unit,
      amount: item.amount,
      foodid: item.foodid,
      nu1name: nutr[1].singleNutrient[0].name,
      nu1amount: nutr[1].singleNutrient[0].amount,
      nu1unit: nutr[1].singleNutrient[0].unit,
      nu2name: nutr[2].singleNutrient[0].name,
      nu2amount: nutr[2].singleNutrient[0].amount,
      nu2unit: nutr[2].singleNutrient[0].unit,
      nu3name: nutr[3].singleNutrient[0].name,
      nu3amount: nutr[3].singleNutrient[0].amount,
      nu3unit: nutr[3].singleNutrient[0].unit
    });
  }

  render() {
    let link = "/nutrition_facts/" + this.state.foodid;
    return (
      <div id="nutrition_component" class="card mb-4 bg-light">
        <Link to={link}>
          <div class="card-body card-header title_text text-dark">
            <h5 class="card-title">
              <H
                id="title_text"
                highlight={this.state.highlighted}
                content={this.state.name.toUpperCase()}
              />
            </h5>
            <p class="card-text">{this.state.calories} Calories</p>
            <p class="card-text">
              {this.state.amount} {this.state.unit}s
            </p>

            <p class="card-text">
              {this.state.nu1amount} {this.state.nu1unit}s {this.state.nu1name}
            </p>
            <p class="card-text">
              {this.state.nu2amount} {this.state.nu2unit}s {this.state.nu2name}
            </p>
            <p class="card-text">
              {this.state.nu3amount} {this.state.nu3unit}s {this.state.nu3name}
            </p>
          </div>
        </Link>
      </div>
    );
  }
}

export default NutritionLandingComponent;
