import React, { Component } from "react";
import Fruit from "../assets/SideFruit.jpg";
import GrapeFruit from "../assets/GrapeFruit.jpg";
import Veggies from "../assets/VeggieStand.jpg";
import Grill from "../assets/Grilling.jpg";
import "../index.css";

class Home extends Component {
  render() {
    return (
      <div id="home_landing">
        <div
          id="carouselExampleControls"
          class="carousel slide"
          data-ride="carousel"
        >
          <div class="carousel-inner carousel-container w-100">
            <div class="carousel-item active">
              <div>
                <div class="centered">
                  <div class="title">unbEATable</div>
                  <p>Promoting healthy and affordable eating</p>
                </div>
                <img src={Fruit} class="w-100 homeImg" alt="..." />
              </div>
            </div>
            <div class="carousel-item">
              <img src={Grill} class="w-100 homeImg" alt="..." />
            </div>
            <div class="carousel-item">
              <img src={Veggies} class="w-100 homeImg" alt="..." />
            </div>
            <div class="carousel-item">
              <img src={GrapeFruit} class="w-100 homeImg" alt="..." />
            </div>
          </div>
          <a
            class="carousel-control-prev"
            href="#carouselExampleControls"
            role="button"
            data-slide="prev"
          >
            <span class="carousel-control-prev-icon" aria-hidden="true" />
            <span class="sr-only">Previous</span>
          </a>
          <a
            class="carousel-control-next"
            href="#carouselExampleControls"
            role="button"
            data-slide="next"
          >
            <span class="carousel-control-next-icon" aria-hidden="true" />
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
    );
  }
}

export default Home;
