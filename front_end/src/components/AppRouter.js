import React, { Component } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  withRouter
} from "react-router-dom";

import About from "./About/About";
import Home from "./Home";
import NavBar from "./NavBar";
import Visualizations from "./Visualizations";

import Recipe from "../pages/RecipeLanding";
import Nutrition from "../pages/NutritionLanding";
import Location from "../pages/LocationsLanding";
import Everything from "../pages/EverythingLanding";

import RecipeModel from "../pages/RecipeModel";
import NutritionModel from "../pages/NutritionModel";
import LocationModel from "../pages/LocationModel";

class AppRouter extends Component {
  constructor() {
    super();
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.setState(nextProps);
    }
  }

  render() {
    return (
      <Router>
        <div>
          <NavBar />

          <Switch>
            <Route
              exact
              path="/recipes/:recipeId"
              render={props => <RecipeModel {...props} />}
            />
            <Route
              exact
              path="/nutrition_facts/:nutritionId"
              render={props => <NutritionModel {...props} />}
            />
            <Route
              exact
              path="/locations/:locationId"
              render={props => <LocationModel {...props} />}
            />

            <Route
              exact
              path="/search/:searchTerm"
              render={props => <Everything {...props} />}
            />

            <Route path="/home" exact component={Home} />
            <Route path="/about" component={About} />
            <Route path="/Visualizations" component={Visualizations} />

            <Route path="/RecipeLand" component={Recipe} />
            <Route path="/NutritionLand" component={Nutrition} />
            <Route path="/LocationLand" component={Location} />

            <Route path="/nav" component={NavBar} />
            <Route path="/" component={Home} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default AppRouter;
