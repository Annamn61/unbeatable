import React, { Component } from "react";
import flame from "../assets/RecipeItems/Flame_grey.png";
import clock from "../assets/RecipeItems/Clock_grey.png";
import plate from "../assets/RecipeItems/Plate_grey.png";
import "../pages/model.css";

class Attributes extends Component {
  constructor(props) {
    super(props);

    this.state = {
      serves: props.servings,
      calories: props.cals,
      time: props.time
    };
  }

  render() {
    return (
      <div>
        <br />
        <div className="bg-secondary line" />
        <br />

        <div className="attributeLine">
          <div className="attBlock">
            <img className="attImg" src={plate} alt="plate" />
            <p>Serves {this.state.serves}</p>
          </div>
          <div className="attBlock">
            <img className="attImg" src={clock} alt="clock" />
            <p>Time</p>
          </div>
          <div className="attBlock">
            <img className="attImg" src={flame} alt="flame" />
            <p>Calories</p>
          </div>
        </div>
        <br />
        <div className="bg-secondary line" />
        <br />
      </div>
    );
  }
}

export default Attributes;
