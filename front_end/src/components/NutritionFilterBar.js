import React, { Component } from "react";
import "../index.css";
import InputRange from "react-input-range";
import "react-input-range/lib/css/index.css";

class NutritionFilterBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sortItems: [
        "Name A-Z",
        "Name Z-A",
        "Calories Low-High",
        "Calories High-Low",
        "Protein Low-High",
        "Protein High-Low"
      ],
      calValue: props.calValue,
      fatValue: props.fatValule,
      carbValue: props.carbValue,
      proteinValue: props.proteinValue,
      sortFunc: props.sortFunc,
      calFunc: props.calFunc,
      fatFunc: props.calFunc,
      carbFunc: props.carbFunc,
      proteinFunc: props.proteinFunc,
      searchFunc: props.searchFunc,
      resetFunc: props.resetFunc
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.setState(nextProps);
    }
  }

  updateSearch() {
    let input = document.getElementById("search_nutrition").value;
    console.log("hello" + input);
    this.state.searchFunc(input);
  }

  sortBy(item) {
    this.state.sortFunc(item);
  }

  filterCal(item) {
    this.state.calFunc(item);
  }

  filterCarb(item) {
    this.state.carbFunc(item);
  }

  filterFat(item) {
    this.state.fatFunc(item);
  }

  filterProtein(item) {
    this.state.proteinFunc(item);
  }

  resetFiltering() {
    this.state.resetFunc();
  }

  render() {
    const sortByTabs = this.state.sortItems.map(item => (
      <a className="dropdown-item diet-item" onClick={() => this.sortBy(item)}>
        {item}
      </a>
    ));

    const noneTab = (
      <a
        className="dropdown-item diet-item"
        onClick={() => this.sortBy("none")}
      >
        none
      </a>
    );

    return (
      <div className="selectorBar">
        <div className="dropdown show">
          <a
            className="btn dropdown-toggle"
            href="#"
            role="button"
            id="dropdownMenuLink"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            Sort by
          </a>

          <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
            {sortByTabs}
            {noneTab}
          </div>
        </div>

        <div class="slideDiv">
          <div>Calorie Density</div>

          <div className="slider">
            <InputRange
              maxValue={500}
              minValue={0}
              value={this.state.calValue}
              onChange={calValue => this.state.calFunc(calValue)}
            />
          </div>
        </div>

        <div class="slideDiv">
          <div>Fat</div>

          <div className="slider">
            <InputRange
              maxValue={20}
              minValue={0}
              value={this.state.fatValue}
              onChange={fatValue => this.state.fatFunc(fatValue)}
            />
          </div>
        </div>

        <div class="slideDiv">
          <div>Carbs</div>

          <div className="slider">
            <InputRange
              maxValue={100}
              minValue={0}
              value={this.state.carbValue}
              onChange={carbValue => this.state.carbFunc(carbValue)}
            />
          </div>
        </div>

        <div class="slideDiv">
          <div>Protein</div>

          <div className="slider">
            <InputRange
              maxValue={20}
              minValue={0}
              value={this.state.proteinValue}
              onChange={proteinValue => this.state.proteinFunc(proteinValue)}
            />
          </div>
        </div>

        <div class="newStyle">
          <button
            type="button"
            className="btn my-2 my-sm-0"
            onClick={() => this.resetFiltering()}
          >
            Reset
          </button>
        </div>

        <div class="newStyle">
          <form class="form-inline">
            <input
              id="search_nutrition"
              className="form-control mr-sm-2"
              placeholder="Search"
              aria-label="Search"
            />
            <button
              type="button"
              id="search_button"
              className="btn my-2 my-sm-0"
              onClick={() => this.updateSearch()}
            >
              Search
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default NutritionFilterBar;
