import React, { Component } from "react";
import "../index.css";
import InputRange from "react-input-range";
import "react-input-range/lib/css/index.css";

class SelectorBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      value: { min: 2, max: 10 },
      filters: props.filters,
      parentGetFilters: props.parentGetFilters,
      diets: props.diets,
      changeDiet: props.changeDiet,
      currDiet: "Category",
      searchString: props.searchString,
      getSearchFieldInput: props.getSearchFieldInput
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props !== nextProps) {
      this.setState(nextProps);
    }
  }

  getFilters() {
    let status = document.getElementsByClassName("custom-control-input");
    let check = new Array(status.length);
    for (let i = 0; i < this.state.filters.length; i++) {
      check[i] = status[i].checked;
    }
    this.state.parentGetFilters(check);
  }

  diet(dietValue) {
    console.log("you chose: " + dietValue);

    this.setState({
      currDiet: dietValue
    });

    this.state.changeDiet(dietValue);
  }

  updateSearch() {
    let searchString = document.getElementById("search_id").value;
    this.setState(
      {
        searchString: searchString
      },
      this.state.getSearchFieldInput(searchString)
    );
    console.log("updateSearchFunc");
  }

  render() {
    const checkboxes = this.state.filters.map((filter, index) => (
      <div className="custom-control custom-checkbox">
        <input
          type="checkbox"
          className="custom-control-input2"
          id={"customCheck" + index}
        />
        <label className="custom-control-label" htmlFor={"customCheck" + index}>
          {filter}
        </label>
      </div>
    ));

    const dietTabs = this.state.diets.map((diet, index) => (
      <a className="dropdown-item diet-item" onClick={() => this.diet(diet)}>
        {diet}
      </a>
    ));

    const noneTab = (
      <a
        className="dropdown-item diet-item"
        onClick={() => this.diet("No Diet")}
      >
        none
      </a>
    );

    return (
      <div className="selectorBar">
        <InputRange
          maxValue={20}
          minValue={0}
          value={this.state.value}
          onChange={value => this.setState({ value })}
        />
        <div>{this.state.value.max}</div>

        <div className="dropdown show">
          <a
            className="btn btn-secondary dropdown-toggle"
            href="#"
            role="button"
            id="dropdownMenuLink"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            {this.state.currDiet}
          </a>

          <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
            {dietTabs}
            {noneTab}
          </div>
        </div>

        <button
          type="button"
          className="btn btn-primary bg-secondary"
          data-toggle="modal"
          data-target="#exampleModal"
        >
          Launch demo modal
        </button>

        <form class="form-inline">
          <input
            id="search_id"
            className="form-control mr-sm-2"
            type="search"
            placeholder="Search"
            aria-label="Search"
          />
          <button
            type="button"
            className="btn btn-outline-success my-2 my-sm-0"
            onClick={() => this.updateSearch()}
          >
            Search
          </button>
        </form>

        <div
          className="modal fade"
          id="exampleModal"
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Select Filters
                </h5>
                <div className="checks modal-body">{checkboxes}</div>
              </div>

              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                  onClick={() => this.getFilters()}
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SelectorBar;
