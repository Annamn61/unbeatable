﻿unbEATable

We at UnbEATable are about one simple thing: letting the people know exactly what they're putting in their bodies. Healthy eating can be a challenge when it's difficult to find nutrition facts for the food on your plate, and we think that affordability shouldn't be a factor that stops you from giving your body what it deserves. By putting emphasis on eating locally, and eating in season, UnbEATable will promote healthy eating that is not only affordable but great for the environment!

Members:\
Jacob Hungerford\
EID: jdh5468\
GitlabID: Jacob_Hung\
Estimated Completion: 15\
Actual Completion: 12

Turner Gregory\
EID: tbg435\
GitlabID: turnergregs\
Estimated Completion: 10\
Actual Completion: 9

Chris Hamill\
EID: wch653\
GitlabID: wchamill\
Estimated Completion: 10\
Actual Completion: 8

Anna Norman\
EID: amn2833\
GitlabID: annamn61\
Estimated Completion: 15\
Actual Completion: 12

Cory Kacal\
EID: csk698\
GitlabID: corykacal\
Estimated Completion: 20\
Actual Completion: 15

https://www.unbeatable-food.com/

Used a previous project's CI file to help us wit postman testing:
https://gitlab.com/CS373-18SU-GRP/IDB/blob/master/.gitlab-ci.yml

Git SHA
36221e51a1f93ba5907c014865604829fdf9104e

Visualizations
We have placed these as links on the Nav Bar
