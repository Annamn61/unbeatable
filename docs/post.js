//The tests below are being run on the request URL above them

//http://www.unbeatable-food.com:5000/api/recipe/page/1

pm.test("response is ok", function() {
  pm.response.to.have.status(200);
});

//http://www.unbeatable-food.com:5000/api/location/page/1

pm.test("response is ok", function() {
  pm.response.to.have.status(200);
});

//http://www.unbeatable-food.com:5000/api/food/page/1

pm.test("response is ok", function() {
  pm.response.to.have.status(200);
});

//http://www.unbeatable-food.com:5000/api/food/1001

pm.test("response is ok", function() {
  pm.response.to.have.status(200);
});

pm.test("Test amount", function() {
  var jsonData = pm.response.json().food;
  pm.expect(jsonData.amount).to.eql(100);
});

pm.test("Test amount", function() {
  var jsonData = pm.response.json().food;
  pm.expect(jsonData.name).to.eql("butter");
});

//http://www.unbeatable-food.com:5000/api/food/1004

pm.test("response is ok", function() {
  pm.response.to.have.status(200);
});

pm.test("Test amount", function() {
  var jsonData = pm.response.json().food;
  pm.expect(jsonData.amount).to.eql(100);
});

pm.test("Test amount", function() {
  var jsonData = pm.response.json().food;
  pm.expect(jsonData.name).to.eql("blue cheese");
});
