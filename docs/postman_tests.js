pm.test("Test 1", function() {
var jsonData = pm.response.json();
pm.expect(jsonData.food.amount).to.eql(100)
})

pm.test(" Test 2, ", function () {
    pm.response.to.have.status(200);
}) 


pm.test("Test 3, ", function () { 
    pm.response.to.not.be.error; 
})

pm.test("Test 4, ", function () {
     // assert that the status code is 200
     pm.response.to.be.ok; // info, success, redirection, clientError,  serverError, are other variants
})

pm.test("Test 5, ", function () {
    pm.response.to.be.withBody
    
})

pm.test("Test 6, ", function () {
    pm.response.to.be.json;
    
})

pm.test(" Test 7, ", function () {
    pm.response.to.not.have.status(800);
}) 

pm.test(" Test 8, ", function () {
    pm.response.to.not.have.status(400);
}) 


pm.test("Test 9", function () {
    pm.expect(pm.response.responseTime).to.be.below(200);
})


pm.test("Test 10", function () {
    pm.expect(pm.response.responseTime).to.be.below(200);
})

pm.test("Test 11", function () {
var jsonData = pm.response.json();
pm.expect(jsonData.food.foodid).to.eql(1004)
})

pm.test("Test 12", function () {
var jsonData = pm.response.json();
pm.expect(jsonData.nutrients[4].name).to.eql("Sugar")
})

pm.test("Test 13", function () {
var jsonData = pm.response.json();
pm.expect(jsonData.nutrients[4].amount).to.eql(.5)
})

pm.test("Test 14", function ()
    {
var jsonData = pm.response.json ();
pm.expect(jsonData.nutrients[8].amount).to.eql(528)
    })
    
    pm.test("Test 15", function ()
    {
var jsonData = pm.response.json ();
pm.expect(jsonData.food.foodid).to.not.eql(-1)
    });

